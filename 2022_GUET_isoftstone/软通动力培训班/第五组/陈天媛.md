# 心得体会

在为期两周的实训中,提高了自己动手做项目的能力,是对能力的进一步锻炼,也是
一种考验,从中获得的诸多收获。首先它锻炼了我做项目的能力,提高了独立思考
问题、自己动手操作的能力,在工作的过程中,复习了以前学习过的知识,并掌握了
一些应用知识的技巧等。在这次的中不仅检验了我所学习的知识,也培养了我的
实践能力,让我知道遇到一个问题,如何去寻找思路,如何去解决问题,最终完成整个
事情。在设计过程中,与同学分工设计,和同学们相互探讨,相互学习,相互监督。学
会了合作,学会了宽容,学会了理解。在这次实训中还锻炼了我其他方面的能力,提
高了我的综合素质。

## 实训期待

通过实训来进一步提升自己的专业素养
.找到自身职业的差距，实训不单是为了落实工作，更包括要明确自己与岗位的差距
以及自己与职业理想的差距，并在实训结束时制定详细可行的补短计划。


## 实训过程



展开为期6.27-7.8的鸿蒙实训计划


## 未来展望
暴富

