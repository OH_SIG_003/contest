package com.itrui.buyitbackend.util;

import com.itrui.buyitbackend.pojo.Product;

import java.util.List;

/**
 * 设置图片域名
 */
public class SetProductPicHeader {

    public static Product setOnlyProduct(Product product,String url){

        int indexOf = Util.getIndexOf(url, "/", 3);
        String header = url.substring(0,indexOf);

        //设置商品主图
        String _proMainPicture = product.getProMainPicture();
        String proMainPicture = header + _proMainPicture;
        product.setProMainPicture(proMainPicture);

        return product;
    }

    public static List<Product> setOnlyProductList(List<Product> products,String url){

        int indexOf = Util.getIndexOf(url, "/", 3);
        String header = url.substring(0,indexOf);

        //设置商品主图
        for (int i = 0; i < products.size();i++){
            String _proMainPicture = products.get(i).getProMainPicture();
            String proMainPicture = header + _proMainPicture;
            products.get(i).setProMainPicture(proMainPicture);

        }

        return products;
    }

    public static Product setProduct(Product product,String url){

        int indexOf = Util.getIndexOf(url, "/", 3);
        String header = url.substring(0,indexOf);

        //设置商品主图
        String _proMainPicture = product.getProMainPicture();
        String proMainPicture = header + _proMainPicture;
        product.setProMainPicture(proMainPicture);

        //设置商品图
        for (int i = 0; i < product.getPictures().size();i++){
            String _picUrl = product.getPictures().get(i).getPicUrl();
            String picUrl = header + _picUrl;
            product.getPictures().get(i).setPicUrl(picUrl);
        }

        return product;
    }


    public static List<Product> setProductList(List<Product> products, String url){

        int indexOf = Util.getIndexOf(url, "/", 3);
        String header = url.substring(0,indexOf);

        //设置商品主图
        for (int i = 0; i < products.size();i++){
            String _proMainPicture = products.get(i).getProMainPicture();
            String proMainPicture = header + _proMainPicture;
            products.get(i).setProMainPicture(proMainPicture);

            //设置商品图
            for (int j = 0; j< products.get(i).getPictures().size();j++){
                String _picUrl = products.get(i).getPictures().get(j).getPicUrl();
                String picUrl = header + _picUrl;
                products.get(i).getPictures().get(j).setPicUrl(picUrl);
            }
        }

        return products;
    }




}
