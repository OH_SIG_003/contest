package com.itrui.buyitbackend.pojo;

import lombok.Data;

@Data
public class AllMessage {

    private Integer messageId;
    private Integer sender;
    private Integer target ;
    private String content;
    private String messageSendTime;
    private Integer type;

}
