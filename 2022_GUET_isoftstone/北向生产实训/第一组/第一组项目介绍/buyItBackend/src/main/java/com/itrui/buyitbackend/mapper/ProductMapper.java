package com.itrui.buyitbackend.mapper;

import com.itrui.buyitbackend.pojo.Business;
import com.itrui.buyitbackend.pojo.Product;
import com.itrui.buyitbackend.pojo.ProductPicture;
import com.itrui.buyitbackend.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface ProductMapper {
    /**
     * 查询全部商品
     * @return
     */
    public List<Product> getAllProducts();

    /**
     * 添加商品
     * @param product
     * @return
     */
    @Insert("insert into product (pro_type, pro_name, price, stock, pro_main_picture, pro_picture,business, pro_createtime, pro_status, introduce)\n" +
            "VALUES (#{proType},#{proName},#{price},#{stock},#{proMainPicture},#{proPicture},#{business.businessAccount},#{proCreatetime},#{proStatus},#{introduce})")
    public int addProduct(Product product);

    /**
     * 添加商品图片
     * @param picture
     * @return
     */
    @Insert("insert into product_picture (pic_url, pro_pic_id) " +
            "VALUES (#{picUrl},#{proId})")
    public int addProductPicture(ProductPicture picture);

    /**
     * 条件查询
     * @param product
     * @return
     */
    public Product getProductByNameAndTypeAndStatus(Product product);

    /**
     * 通过商品名查询
     * @param name
     * @return
     */
    public List<Product> getProductByName(@Param("name") String name);

    /**
     * 通过商品类查找商品
     * @param type
     * @return
     */
    public List<Product> getProductByType(@Param("type") String type);

    /**
     * 通过id删除商品
     * @param id
     * @return
     */
    public int delProductById(@Param("id") Integer id);

    /**
     * 删除商品对应图片信息
     * @param picId
     * @return
     */
    public int delProductPic(@Param("picId") Integer picId);

    /**
     * 修改商品信息
     * @param product
     * @return
     */
    public int updateProduct(Product product);

    /**
     * 通过商品id查找
     * @param id
     * @return
     */
    public Product getProductById(@Param("id") Integer id);

}
