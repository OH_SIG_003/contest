package com.itrui.buyitbackend.common;

import lombok.Data;

@Data
public class Result {
    private Object body;
    private Integer code;
    private String msg;

    public Result(Integer code,Object body) {
        this.body = body;
        this.code = code;
    }

    public Result(Integer code, Object body, String msg) {
        this.body = body;
        this.code = code;
        this.msg = msg;
    }

    public Result(Integer code,String msg) {
        this.code = code;
        this.msg = msg;
    }
}

