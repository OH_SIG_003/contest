上午进行开发环境安装：
1. 安装WSL2（一个在Windows 10上能够运行原生Linux二进制可执行文件（ELF格式）的兼容层，其目标是使纯正的Ubuntu、Debian等映像能下载和解压到用户的本地计算机，并且映像内的工具和实用工具能在此子系统上原生运行。）
1.1管理员身份打开 PowerShell，输入以下命令:
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
1.2启用“虚拟机平台”，在PowerShell上输入如下指令:
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
注意:操作完之后然后将电脑重启
1.3使用默认方式对wsl_update_x64.msi安装
2.在WSL2上导入Ubuntu20.04后，即可用wsl访问linux系统。
3. 安装docker桌面版，安装好docker之后还需要设置wsl2运行时的内存，导入docker镜像（docker-desktop-data.tar和docker-desktop.tar文件）
4.运行docker镜像，执行docker run来运行容器。用exit来退出容器的运行。
5.gitee工具进行项目开源的操作演示。

下午进行开发环境的使用：
1、安装gitee和TortoiseGit，下载openHormony源码。

2、安装Visual Studio Code，运行docker环境，进行后续调试学习。
2.1、qemu轻量系统选择开发板、编译与执行（docker控制台终端 输入:hb set指令,再输入回车,到达选择开发板的界面,用鼠标或键盘上下键选择qemu_mini_system_demo。
hb build -f  编译
./qemu-run  运行）
2.2  源码讲解运行（时间tick默认=10ms，修改代码注意修改BUILD.gn文件，在main.c文件里调用对应方法）：

时间定时器  soft_timer_demo()

任务调度，多任务、单任务（任务优先级）  task_example()

信号量，互斥锁  semaphore_example（）