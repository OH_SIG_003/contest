/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/prctl.h>

#include "sample_comm_nnie.h"
#include "sample_media_ai.h"
#include "ai_infer_process.h"
#include "yolov2_hand_detect.h"
#include "vgs_img.h"
#include "ive_img.h"
#include "misc_util.h"
#include "hisignalling.h"
#include "posix_help.h"
#include "audio_aac_adp.h"
#include "base_interface.h"
#include "osd_img.h"
#include "hand_classify.h"
#include "sample_ai_main.h"
#include "sample_audio_part.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

#define HAND_FRM_WIDTH     640
#define HAND_FRM_HEIGHT    384
#define DETECT_OBJ_MAX     32
#define RET_NUM_MAX        4
#define DRAW_RETC_THICK    8    // Draw the width of the line
#define WIDTH_LIMIT        32
#define HEIGHT_LIMIT       32
#define IMAGE_WIDTH        224  // The resolution of the model IMAGE sent to the classification is 224*224
#define IMAGE_HEIGHT       224
#define MODEL_FILE_GESTURE    "/userdata/models/hand_classify/card_classify_yolov2.wk" // darknet framework wk model
#define MODEL_FILE_MIX        "/userdata/models/hand_classify/round_card_classify.wk"//"/userdata/models/hand_classify/mix_classify_yolov2.wk"
#define AUDIO_FRAME        14
#define MULTIPLE_OF_EXPANSION 100   // Multiple of expansion
#define SCORE_MAX           4096
#define AUDIO_SCORE        40       // Confidence can be configured by yourself
#define UNKOWN_ROUND_CARD     3    // Unkown Round Card
#define UNKOWN_RECT_CARD     20
#define BUFFER_SIZE           16    // buffer size
#define TXT_BEGX            40
#define TXT_BEGY            40
#define THRESH_MIN          30      // Acceptable probability threshold (over this value will be returned to the app)


static int biggestBoxIndex;
static IVE_IMAGE_S img;
static DetectObjInfo objs[DETECT_OBJ_MAX] = {0};
static RectBox boxs[DETECT_OBJ_MAX] = {0};
static RectBox objBoxs[DETECT_OBJ_MAX] = {0};
static RectBox remainingBoxs[DETECT_OBJ_MAX] = {0};
static RectBox cnnBoxs[DETECT_OBJ_MAX] = {0}; // Store the results of the classification network
static RecogNumInfo numInfo[RET_NUM_MAX] = {0};
static IVE_IMAGE_S imgIn;
static IVE_IMAGE_S imgDst;
static VIDEO_FRAME_INFO_S frmIn;
static VIDEO_FRAME_INFO_S frmDst;
int uartFd = 0;
static int g_num = 108;
static HI_BOOL g_bAudioProcessStopSignal = HI_FALSE;

static int g_count = 0;
static OsdSet* g_osdsTrash = NULL;
static HI_S32 g_osd0Trash = -1;
static pthread_t g_audioProcessThread = 0;
static pthread_t g_vi_vo_audioProcessThread = 0;

static SkPair g_stmChn = {
    .in = -1,
    .out = -1
};

static HI_VOID PlayAudio(const RecogNumInfo items)
{
    if  (g_count < AUDIO_FRAME) {
        g_count++;
        return;
    }

    const RecogNumInfo *item = &items;
    uint32_t score = item->score * MULTIPLE_OF_EXPANSION / SCORE_MAX;
    if ((score > AUDIO_SCORE) && (g_num != item->num)) {
        if(model_mode == 0){
            g_num = item->num;
        }
        else if(model_mode == 1){
            g_num = item->num;
            g_num = g_num + 11; // 9+11 invalid
        }
        
        if (((g_num != UNKOWN_ROUND_CARD) && (model_mode == 0)) || ((g_num  != UNKOWN_RECT_CARD) && (model_mode == 1)) ) {
            AudioTest(g_num, -1);
        }
    }
    g_count = 0;
}

static HI_VOID* GetAudioFileName(HI_VOID* arg)
{
    RecogNumInfo numInfo = {0};
    int ret;

    while (g_bAudioProcessStopSignal == false) {
        ret = FdReadMsg(g_stmChn.in, &numInfo, sizeof(RecogNumInfo));
        if (ret == sizeof(RecogNumInfo)) {
            PlayAudio(numInfo);
        }
    }

    return NULL;
}

HI_S32 Yolo2HandDetectResnetClassifyLoad(uintptr_t* model)
{
    SAMPLE_SVP_NNIE_CFG_S *self = NULL;
    HI_S32 ret;

    ret = CnnCreate(&self, MODEL_FILE_GESTURE);
    *model = ret < 0 ? 0 : (uintptr_t)self;
    HandDetectInit(); // Initialize the hand detection model
    SAMPLE_PRT("Load hand detect claasify model success\n");
    /* uart open init */
    uartFd = UartOpenInit();
    if (uartFd < 0) {
        printf("uart1 open failed\r\n");
    } else {
        printf("uart1 open successed\r\n");
    }
    return ret;
}

HI_S32 Yolo2MixDetectResnetClassifyLoad(uintptr_t* model, OsdSet* osds)
{
    SAMPLE_SVP_NNIE_CFG_S *self = NULL;
    HI_S32 ret;
    HI_CHAR audioThreadName[BUFFER_SIZE] = {0};

    ret = OsdLibInit();
    HI_ASSERT(ret == HI_SUCCESS);

    g_osdsTrash = osds;
    HI_ASSERT(g_osdsTrash);
    g_osd0Trash = OsdsCreateRgn(g_osdsTrash);
    HI_ASSERT(g_osd0Trash >= 0);

    if(model_mode == 0){
        ret = CnnCreate(&self, MODEL_FILE_MIX);
        SAMPLE_PRT("model_mode == 0!!!!!!!!!!!!!!!!!!!!! Round Card\n");
    }
    else if(model_mode == 1){
        ret = CnnCreate(&self, MODEL_FILE_GESTURE);
        SAMPLE_PRT("model_mode == 1?????????????????????? Rectangle Card\n");
    }

    *model = ret < 0 ? 0 : (uintptr_t)self;
    MixDetectInit(); // Initialize the hand detection model
    SAMPLE_PRT("Load mix detect claasify model success\n");

    if (GetCfgBool("audio_player:support_audio", true)) {
        ret = SkPairCreate(&g_stmChn);
        HI_ASSERT(ret == 0);
        if (snprintf_s(audioThreadName, BUFFER_SIZE, BUFFER_SIZE - 1, "AudioProcess") < 0) {
            HI_ASSERT(0);
        }
        prctl(PR_SET_NAME, (unsigned long)audioThreadName, 0, 0, 0);
        if(model_mode == 0){
            ret = pthread_create(&g_audioProcessThread, NULL, GetAudioFileName, NULL);
        }
        else if(model_mode == 1){
            ret = pthread_create(&g_vi_vo_audioProcessThread, NULL, SAMPLE_AUDIO_AiAo, NULL);
        }
        if (ret != 0) {
            SAMPLE_PRT("audio proccess thread creat fail:%s\n", strerror(ret));
            return ret;
        }
    }

    /* uart open init */
    uartFd = UartOpenInit();
    if (uartFd < 0) {
        printf("uart1 open failed\r\n");
    } else {
        printf("uart1 open successed\r\n");
    }
    return ret;
}

HI_S32 Yolo2HandDetectResnetClassifyUnload(uintptr_t model)
{
    CnnDestroy((SAMPLE_SVP_NNIE_CFG_S*)model);
    HandDetectExit(); // Uninitialize the hand detection model
    SAMPLE_PRT("Unload hand detect claasify model success\n");

    return 0;
}

HI_S32 Yolo2MixDetectResnetClassifyUnload(uintptr_t model)
{
    CnnDestroy((SAMPLE_SVP_NNIE_CFG_S*)model);
    MixDetectExit(); // Uninitialize the hand detection model
    SAMPLE_PRT("Unload mix detect claasify model success\n");
    OsdsClear(g_osdsTrash);

    if (GetCfgBool("audio_player:support_audio", true)) {
        SkPairDestroy(&g_stmChn);
        SAMPLE_PRT("SkPairDestroy success\n");
        g_bAudioProcessStopSignal = HI_TRUE;
        if(model_mode == 0){
            pthread_join(g_audioProcessThread, NULL);
            g_audioProcessThread = 0;
        }
        else if(model_mode == 1){
            pthread_join(g_vi_vo_audioProcessThread, NULL);
            g_vi_vo_audioProcessThread = 0;
        }
    }

    return 0;
}

/* Get the maximum hand */
static HI_S32 GetBiggestHandIndex(RectBox boxs[], int detectNum)
{
    HI_S32 handIndex = 0;
    HI_S32 biggestBoxIndex = handIndex;
    HI_S32 biggestBoxWidth = boxs[handIndex].xmax - boxs[handIndex].xmin + 1;
    HI_S32 biggestBoxHeight = boxs[handIndex].ymax - boxs[handIndex].ymin + 1;
    HI_S32 biggestBoxArea = biggestBoxWidth * biggestBoxHeight;

    for (handIndex = 1; handIndex < detectNum; handIndex++) {
        HI_S32 boxWidth = boxs[handIndex].xmax - boxs[handIndex].xmin + 1;
        HI_S32 boxHeight = boxs[handIndex].ymax - boxs[handIndex].ymin + 1;
        HI_S32 boxArea = boxWidth * boxHeight;
        if (biggestBoxArea < boxArea) {
            biggestBoxArea = boxArea;
            biggestBoxIndex = handIndex;
        }
        biggestBoxWidth = boxs[biggestBoxIndex].xmax - boxs[biggestBoxIndex].xmin + 1;
        biggestBoxHeight = boxs[biggestBoxIndex].ymax - boxs[biggestBoxIndex].ymin + 1;
    }

    if ((biggestBoxWidth == 1) || (biggestBoxHeight == 1) || (detectNum == 0)) {
        biggestBoxIndex = -1;
    }

    return biggestBoxIndex;
}

/* hand gesture recognition info */
// static void HandDetectFlag(const RecogNumInfo resBuf)
// {
//     HI_CHAR *gestureName = NULL;
//     switch (resBuf.num) {
//         case 0u:
//             gestureName = "gesture fist 0";
//             UartSendRead(uartFd, FistGesture); // 拳头手势
//             SAMPLE_PRT("----gesture name----:%s\n", gestureName);
//             break;
//         case 1u:
//             gestureName = "gesture indexUp 1";
//             UartSendRead(uartFd, ForefingerGesture); // 食指手势
//             SAMPLE_PRT("----gesture name----:%s\n", gestureName);
//             break;
//         case 2u:
//             gestureName = "gesture OK 2";
//             UartSendRead(uartFd, OkGesture); // OK手势
//             SAMPLE_PRT("----gesture name----:%s\n", gestureName);
//             break;
//         case 3u:
//             gestureName = "gesture palm 3";
//             UartSendRead(uartFd, PalmGesture); // 手掌手势
//             SAMPLE_PRT("----gesture name----:%s\n", gestureName);
//             break;
//         case 4u:
//             gestureName = "gesture yes 4";
//             UartSendRead(uartFd, YesGesture); // yes手势
//             SAMPLE_PRT("----gesture name----:%s\n", gestureName);
//             break;
//         case 5u:
//             gestureName = "gesture pinchOpen 5";
//             UartSendRead(uartFd, ForefingerAndThumbGesture); // 食指 + 大拇指
//             SAMPLE_PRT("----gesture name----:%s\n", gestureName);
//             break;
//         case 6u:
//             gestureName = "gesture phoneCall 6";
//             UartSendRead(uartFd, LittleFingerAndThumbGesture); // 大拇指 + 小拇指
//             SAMPLE_PRT("----gesture name----:%s\n", gestureName);
//             break;
//         case 7u:
//             gestureName = "gesture phoneCall 7";
//             UartSendRead(uartFd, LittleFingerAndThumbGesture); // 大拇指 + 小拇指
//             SAMPLE_PRT("----gesture name----:%s\n", gestureName);
//             break;
//         case 8u:
//             gestureName = "gesture phoneCall 8";
//             UartSendRead(uartFd, LittleFingerAndThumbGesture); // 大拇指 + 小拇指
//             SAMPLE_PRT("----gesture name----:%s\n", gestureName);
//             break;
//         case 9u:
//             gestureName = "gesture phoneCall 9";
//             UartSendRead(uartFd, LittleFingerAndThumbGesture); // 大拇指 + 小拇指
//             SAMPLE_PRT("----gesture name----:%s\n", gestureName);
//             break;
//         default:
//             gestureName = "gesture others -1";
//             UartSendRead(uartFd, InvalidGesture); // 无效值
//             SAMPLE_PRT("----gesture name----:%s\n", gestureName);
//             break;
//     }
//     SAMPLE_PRT("hand gesture success\n");
// }

// /* hand gesture recognition info */
// HI_S32 MixDetectFlag(const RecogNumInfo resBuf)
// {
//     HI_CHAR *objectName = NULL;
//     HI_S32 times = 0;
//     switch (resBuf.num) {
//         case 0u:
//             objectName = "Card 0";
//             UartSendRead(uartFd, FistGesture); // 方形识字卡
//             SAMPLE_PRT("----object name----:%s\n", objectName);
//             return 0;
//         case 1u:
//             objectName = "Round 1";
//             UartSendRead(uartFd, ForefingerGesture); // 圆形识字卡
//             SAMPLE_PRT("----object name----:%s\n", objectName);
//             return 1;
//         case 2u:
//             objectName = "Book 2";
//             UartSendRead(uartFd, OkGesture); // 图书1
//             SAMPLE_PRT("----object name----:%s\n", objectName);
//             return 2;
//         default:
//             objectName = "others -1";
//             UartSendRead(uartFd, InvalidGesture); // 无效值
//             SAMPLE_PRT("----object name----:%s\n", objectName);
//             return 100;
//     }
//     SAMPLE_PRT("mix detection success\n");
// }

static HI_S32 MixDetectFlag(const RecogNumInfo items[], HI_S32 itemNum, HI_CHAR* buf, HI_S32 size)
{
    HI_S32 offset = 0;
    HI_CHAR *Name = NULL;

    offset += snprintf_s(buf + offset, size - offset, size - offset - 1, "rectangle card classify: {");
    for (HI_U32 i = 0; i < itemNum; i++) {
        const RecogNumInfo *item = &items[i];
        uint32_t score = item->score * HI_PER_BASE / SCORE_MAX;
        if (score < THRESH_MIN) {
            break;
        }
        SAMPLE_PRT("----rectangle card item flag----num:%d, score:%d\n", item->num, score);
        switch (item->num) {
        case 0u:
            Name = "picture turtle 0";
            UartSendRead(uartFd, picture_turtle); 
            SAMPLE_PRT("----rectangle card name----:%s\n", Name);
            break;
        case 1u:
            Name = "picture stone 1";
            UartSendRead(uartFd, picture_stone);
            SAMPLE_PRT("----rectangle card name----:%s\n", Name);
            break;
        case 2u:
            Name = "picture cave 2";
            UartSendRead(uartFd, picture_cave);
            SAMPLE_PRT("----rectangle card name----:%s\n", Name);
            break;
        case 3u:
            Name = "picture water 3";
            UartSendRead(uartFd, picture_water);
            SAMPLE_PRT("----rectangle card name----:%s\n", Name);
            break;
        case 4u:
            Name = "character water 4";
            UartSendRead(uartFd, character_water);
            SAMPLE_PRT("----rectangle card name----:%s\n", Name);
            break;
        case 5u:
            Name = "character cave 5";
            UartSendRead(uartFd, character_cave);
            SAMPLE_PRT("----rectangle card name----:%s\n", Name);
            break;
        case 6u:
            Name = "character turtle 6";
            UartSendRead(uartFd, character_turtle);
            SAMPLE_PRT("----rectangle card name----:%s\n", Name);
            break;
        case 7u:
            Name = "character bear 7";
            UartSendRead(uartFd, character_bear);
            SAMPLE_PRT("----rectangle card name----:%s\n", Name);
            break;
        case 8u:
            Name = "character stone 8";
            UartSendRead(uartFd, character_stone);
            SAMPLE_PRT("----rectangle card name----:%s\n", Name);
            break;
        default:
            Name = "InvalidRectangleCard -1";
            UartSendRead(uartFd, InvalidRectangleCard);
            SAMPLE_PRT("----rectangle card name----:%s\n", Name);
            break;
        }
        offset += snprintf_s(buf + offset, size - offset, size - offset - 1,
            "%s%s %u:%u%%", (i == 0 ? " " : ", "), Name, (int)item->num, (int)score);
        HI_ASSERT(offset < size);
    }
    offset += snprintf_s(buf + offset, size - offset, size - offset - 1, " }");
    HI_ASSERT(offset < size);
    return HI_SUCCESS;
}

static HI_S32 CnnTrashClassifyFlag(const RecogNumInfo items[], HI_S32 itemNum, HI_CHAR* buf, HI_S32 size)
{
    HI_S32 offset = 0;
    HI_CHAR *Name = NULL;

    offset += snprintf_s(buf + offset, size - offset, size - offset - 1, "round card classify: {");
    for (HI_U32 i = 0; i < itemNum; i++) {
        const RecogNumInfo *item = &items[i];
        uint32_t score = item->score * HI_PER_BASE / SCORE_MAX;
        if (score < THRESH_MIN) {
            break;
        }
        SAMPLE_PRT("----round card item flag----num:%d, score:%d\n", item->num, score);
        switch (item->num) {
            case 5u:
                Name = "RedOwl 5";
                UartSendRead(uartFd, RedOwl);
                SAMPLE_PRT("----round card name----:%s\n", Name);
                break;
            case 8u:
                Name = "WhiteDog 8";
                UartSendRead(uartFd, WhiteDog);
                SAMPLE_PRT("----round card name----:%s\n", Name);
                break;
            case 2u:
                Name = "Hedgehog 2";
                UartSendRead(uartFd, Hedgehog);
                SAMPLE_PRT("----round card name----:%s\n", Name);
                break;
            case 7u:
                Name = "TwoRaccoon 7";
                UartSendRead(uartFd, TwoRaccoon);
                SAMPLE_PRT("----round card name----:%s\n", Name);
                break;
            case 0u:
                Name = "Bear 0";
                UartSendRead(uartFd, Bear);
                SAMPLE_PRT("----round card name----:%s\n", Name);
                break;
            case 10u:
                Name = "YellowDonkey 10";
                UartSendRead(uartFd, YellowDonkey);
                SAMPLE_PRT("----round card name----:%s\n", Name);
                break;
            case 6u:
                Name = "Toucan 6";
                UartSendRead(uartFd, Toucan);
                SAMPLE_PRT("----round card name----:%s\n", Name);
                break;
            case 1u:
                Name = "BrownOwl 1";
                UartSendRead(uartFd, BrownOwl);
                SAMPLE_PRT("----round card name----:%s\n", Name);
                break;
            case 4u:
                Name = "Pig 4";
                UartSendRead(uartFd, Pig);
                SAMPLE_PRT("----round card name----:%s\n", Name);
                break;
            case 9u:
                Name = "YellowDog 9";
                UartSendRead(uartFd, YellowDog);
                SAMPLE_PRT("----round card name----:%s\n", Name);
                break;

            case 3u:
                Name = "InvalidRoundCard 3";
                UartSendRead(uartFd, InvalidRoundCard);
                SAMPLE_PRT("----round card name----:%s\n", Name);
                break;
        }
        offset += snprintf_s(buf + offset, size - offset, size - offset - 1,
            "%s%s %u:%u%%", (i == 0 ? " " : ", "), Name, (int)item->num, (int)score);
        HI_ASSERT(offset < size);
    }
    offset += snprintf_s(buf + offset, size - offset, size - offset - 1, " }");
    HI_ASSERT(offset < size);
    return HI_SUCCESS;
}

HI_S32 Yolo2HandDetectResnetClassifyCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm)
{
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    HI_S32 resLen = 0;
    int objNum;
    int ret;
    int num = 0;

    ret = FrmToOrigImg((VIDEO_FRAME_INFO_S*)srcFrm, &img);
    SAMPLE_CHECK_EXPR_RET(ret != HI_SUCCESS, ret, "hand detect for YUV Frm to Img FAIL, ret=%#x\n", ret);

    objNum = HandDetectCal(&img, objs); // Send IMG to the detection net for reasoning
    for (int i = 0; i < objNum; i++) {
        cnnBoxs[i] = objs[i].box;
        RectBox *box = &objs[i].box;
        RectBoxTran(box, HAND_FRM_WIDTH, HAND_FRM_HEIGHT,
            dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height);
        SAMPLE_PRT("yolo2_out: {%d, %d, %d, %d}\n", box->xmin, box->ymin, box->xmax, box->ymax);
        boxs[i] = *box;
    }
    biggestBoxIndex = GetBiggestHandIndex(boxs, objNum);
    SAMPLE_PRT("biggestBoxIndex:%d, objNum:%d\n", biggestBoxIndex, objNum);

    // When an object is detected, a rectangle is drawn in the DSTFRM
    if (biggestBoxIndex >= 0) {
        objBoxs[0] = boxs[biggestBoxIndex];
        MppFrmDrawRects(dstFrm, objBoxs, 1, RGB888_GREEN, DRAW_RETC_THICK); // Target hand objnum is equal to 1

        for (int j = 0; (j < objNum) && (objNum > 1); j++) {
            if (j != biggestBoxIndex) {
                remainingBoxs[num++] = boxs[j];
                // others hand objnum is equal to objnum -1
                MppFrmDrawRects(dstFrm, remainingBoxs, objNum - 1, RGB888_RED, DRAW_RETC_THICK);
            }
        }

        // Crop the image to classification network
        ret = ImgYuvCrop(&img, &imgIn, &cnnBoxs[biggestBoxIndex]);
        SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "ImgYuvCrop FAIL, ret=%#x\n", ret);

        if ((imgIn.u32Width >= WIDTH_LIMIT) && (imgIn.u32Height >= HEIGHT_LIMIT)) {
            COMPRESS_MODE_E enCompressMode = srcFrm->stVFrame.enCompressMode;
            ret = OrigImgToFrm(&imgIn, &frmIn);
            frmIn.stVFrame.enCompressMode = enCompressMode;
            SAMPLE_PRT("crop u32Width = %d, img.u32Height = %d\n", imgIn.u32Width, imgIn.u32Height);
            ret = MppFrmResize(&frmIn, &frmDst, IMAGE_WIDTH, IMAGE_HEIGHT);
            ret = FrmToOrigImg(&frmDst, &imgDst);
            ret = CnnCalU8c1Img(self,  &imgDst, numInfo, sizeof(numInfo) / sizeof((numInfo)[0]), &resLen);
            SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "CnnCalU8c1Img FAIL, ret=%#x\n", ret);
            HI_ASSERT(resLen <= sizeof(numInfo) / sizeof(numInfo[0]));
            //HandDetectFlag(numInfo[0]);
            MppFrmDestroy(&frmDst);
        }
        IveImgDestroy(&imgIn);
    }

    return ret;
}

HI_S32 Yolo2MixDetectResnetClassifyCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm)
{
    SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
    HI_S32 resLen = 0;
    HI_S32 choice = 0;
    int objNum;
    int ret;
    int num = 0;
    static HI_CHAR prevOsd[NORM_BUF_SIZE] = "";
    HI_CHAR osdBuf[NORM_BUF_SIZE] = "";

    ret = FrmToOrigImg((VIDEO_FRAME_INFO_S*)srcFrm, &img);
    SAMPLE_CHECK_EXPR_RET(ret != HI_SUCCESS, ret, "mix detect for YUV Frm to Img FAIL, ret=%#x\n", ret);

    objNum = MixDetectCal(&img, objs); // Send IMG to the detection net for reasoning
    for (int i = 0; i < objNum; i++) {
        cnnBoxs[i] = objs[i].box;
        RectBox *box = &objs[i].box;
        RectBoxTran(box, HAND_FRM_WIDTH, HAND_FRM_HEIGHT,
            dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height);
        SAMPLE_PRT("yolo2_out: {%d, %d, %d, %d}\n", box->xmin, box->ymin, box->xmax, box->ymax);
        boxs[i] = *box;
    }
    biggestBoxIndex = GetBiggestHandIndex(boxs, objNum);
    SAMPLE_PRT("biggestBoxIndex:%d, objNum:%d\n", biggestBoxIndex, objNum);

    // When an object is detected, a rectangle is drawn in the DSTFRM
    if (biggestBoxIndex >= 0) {
        objBoxs[0] = boxs[biggestBoxIndex];
        MppFrmDrawRects(dstFrm, objBoxs, 1, RGB888_GREEN, DRAW_RETC_THICK); // Target hand objnum is equal to 1

        for (int j = 0; (j < objNum) && (objNum > 1); j++) {
            if (j != biggestBoxIndex) {
                remainingBoxs[num++] = boxs[j];
                // others hand objnum is equal to objnum -1
                MppFrmDrawRects(dstFrm, remainingBoxs, objNum - 1, RGB888_RED, DRAW_RETC_THICK);
            }
        }

        // Crop the image to classification network
        ret = ImgYuvCrop(&img, &imgIn, &cnnBoxs[biggestBoxIndex]);
        SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "ImgYuvCrop FAIL, ret=%#x\n", ret);


        if ((imgIn.u32Width >= WIDTH_LIMIT) && (imgIn.u32Height >= HEIGHT_LIMIT)) {
            COMPRESS_MODE_E enCompressMode = srcFrm->stVFrame.enCompressMode;
            ret = OrigImgToFrm(&imgIn, &frmIn);
            frmIn.stVFrame.enCompressMode = enCompressMode;
            SAMPLE_PRT("crop u32Width = %d, img.u32Height = %d\n", imgIn.u32Width, imgIn.u32Height);
            ret = MppFrmResize(&frmIn, &frmDst, IMAGE_WIDTH, IMAGE_HEIGHT);
            ret = FrmToOrigImg(&frmDst, &imgDst);
            ret = CnnCalU8c1Img(self,  &imgDst, numInfo, sizeof(numInfo) / sizeof((numInfo)[0]), &resLen);
            SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "CnnCalU8c1Img FAIL, ret=%#x\n", ret);
            HI_ASSERT(resLen <= sizeof(numInfo) / sizeof(numInfo[0]));

            if(model_mode == 0){
                ret = CnnTrashClassifyFlag(numInfo, resLen, osdBuf, sizeof(osdBuf));
            }
            else if(model_mode == 1){
                ret = MixDetectFlag(numInfo, resLen, osdBuf, sizeof(osdBuf));
            }

            if (GetCfgBool("audio_player:support_audio", true)) {
                if (FdWriteMsg(g_stmChn.out, &numInfo[0], sizeof(RecogNumInfo)) != sizeof(RecogNumInfo)) {
                    SAMPLE_PRT("FdWriteMsg FAIL\n");
                }
            }       

            if (strcmp(osdBuf, prevOsd) != 0) {
                HiStrxfrm(prevOsd, osdBuf, sizeof(prevOsd));

                // Superimpose graphics into resFrm
                HI_OSD_ATTR_S rgn;
                TxtRgnInit(&rgn, osdBuf, TXT_BEGX, TXT_BEGY, ARGB1555_WHITE); // font width and heigt use default 40
                OsdsSetRgn(g_osdsTrash, g_osd0Trash, &rgn);
                ret = HI_MPI_VPSS_SendFrame(0, 0, srcFrm, 0);
                if (ret != HI_SUCCESS) {
                    SAMPLE_PRT("Error(%#x), HI_MPI_VPSS_SendFrame failed!\n", ret);
                }
            }

        
            MppFrmDestroy(&frmDst);
        }
        IveImgDestroy(&imgIn);
    }

    return ret;
}

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */
