#ifndef _ASTARSEARCH_H
#define _ASTARSEARCH_H

#include <Eigen/Eigen>

void AstarGraphSearch(Eigen::Vector2d start_pt, Eigen::Vector2d end_pt);
std::vector<Eigen::Vector2d> getPath(void);
void resetUsedGrids(void);

#endif