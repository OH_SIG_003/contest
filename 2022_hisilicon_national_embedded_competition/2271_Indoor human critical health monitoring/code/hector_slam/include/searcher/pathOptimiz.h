#ifndef _PATHOPTIMIZ_H
#define _PATHOPTIMIZ_H

#include<Eigen/Eigen>
Eigen::VectorXd timeAllocation( Eigen::MatrixXd Path);

Eigen::MatrixXd PolyQPGeneration(const int d_order,const Eigen::MatrixXd &Path,const Eigen::MatrixXd &Vel,const Eigen::MatrixXd &Acc,const Eigen::VectorXd &Time);

#endif
