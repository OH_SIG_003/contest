#ifndef KITWS2812_H
#define KITWS2812_H

typedef enum {
    LED_WAKE_UP = 1,
    LED_BREATH,
    LED_EXIT,
};
void Kit_LED_init(void);
void Kit_LED_State(int *state);
void Kit_LED_ROLL_WAKE_UP(void);

#endif