#define mstime =72000;

static const char PLATE1[][32]={
   {"luAD06666"},//鲁D.D06666   1号
   {"huAD00806"}, //沪A.D00806 2号
   {"wanAD06755"},  //皖A.D06755   3号
   {"chuanAF14888"},//川A.F14888  4号
   {"zheBD08889"},  //浙B.D08889 5号
   {"suBF01188"},  //苏B.F01188  6号
   //{"AD35169"},  //皖A.D35169

  // {"yueAD12345"},//粤A.D12345
  // {"chuanE45678D"},//川E.45678D
  // {"wanCF98765"},//皖C.F98765
   //{"!\"A.D45678"}, //京B.D45678
   // {"?@F.34567D"},//苏F.34567D
} ;

static const char PLATE2[][32]={
   {"]^A.D06666"},//鲁A.06666  1号
   {"=>A.D00806"}, //沪A.D00806 2号
   {"WXA.D06755"},  //皖A.D06755 3号
   {"mnA.F14888"},//川A.F14888  4号
   {"UVB.D08889"},  //浙B.D08889 5号
   {"?@B.F01188"},  //苏B.F01188  6号
  // {"WXA.D35169"},  //皖A.D35169
  // {"efA.D12345"},//粤A.D12345
  // {"mnE.45678D"},//川E.45678D
  // {"WXC.F98765"},//皖C.F98765
   
   //{"!\"A.D45678"}, //京B.D45678
   // {"?@F.34567D"},//苏F.34567D
} ;

struct VLP{
   char plate[13];
   int  parktime;
   float  cost;
   int  gpio;
};//vlp1;//,vlp2,vlp3,vlp4;

//static int plate_size = sizeof(PLATE)/sizeof(PLATE[0]);
//static int GPIO[]={8,7,12,11,5,10};
static int GPIO[]={10,5,11,12,7,8};

//static int gpio_size = sizeof(GPIO);
