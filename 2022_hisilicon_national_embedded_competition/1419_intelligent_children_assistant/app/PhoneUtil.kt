package com.tangjinghao.monitor.util

import android.content.Context

/**
 * @Author 唐靖豪
 * @Date 2022/7/12 17:57
 * @Email 762795632@qq.com
 * @Description
 */

object PhoneUtil {

    /**
    * 判断手机处于什么模式
    */
    fun getPhoneMode(context: Context):Int{
        return if(context.applicationContext.resources.configuration.uiMode==0x21){
            Constants.DARK_MODE
        }else{
            Constants.LIGHT_MODE
        }
    }

    fun formatTime(ms: Long): String{
        val ss = 1000
        val mi = ss * 60
        val hh = mi * 60
        val dd = hh * 24
        val day = ms / dd
        val hour = (ms - day * dd) / hh
        val minute = (ms - day * dd - hour * hh) / mi
        val second = (ms - day * dd - hour * hh - minute * mi) / ss
        val milliSecond = ms - day * dd - hour * hh - minute * mi - second * ss
        val sb = StringBuffer()
        if (day > 0) {
            sb.append(day.toString() + "天")
        }
        if (hour > 0) {
            sb.append(hour.toString() + "小时")
        }
        if (minute > 0) {
            sb.append(minute.toString() + "分")
        }
        if (second > 0) {
            sb.append(second.toString() + "秒")
        }
        return sb.toString()
    }

}