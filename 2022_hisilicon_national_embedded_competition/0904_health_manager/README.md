随着生活越来越好，人们开始适度控制热量摄入并养成健康的饮食习惯。然而大部分人对食物的热量及营养成分并不了解，这使得他们无法对入口的食物进行定量精准的计算。因此我们团队设计该饮食智能助手的目的是能帮助用户精准测算食物热量和三大营养素（碳水化合物，蛋白质，脂肪）的含量，帮助用户更好地合理制定饮食计划。

 健康饮食助手拥有三大技术特点。首先是3516套件基于AI分类网对待测食物进行图像采集，识别与分类，并将信息发送到3861套件。然后是，3861套件基于该食物的重量进行营养信息的分析与计算，并将计算结果显示到OLED显示屏上。最后是3861套件的NFC功能，使用手机登录相对应的微信小程序即可接收来自3861套件计算后的具体食物营养信息。

 3861套件中的电子秤元件能够测量5kg以内的重量，基本满足所有食物的称重需求。3516套件的分类网通过图像AI学习，能够在3s内快速检测并识别上百种日常食物，包括：蔬菜，水果和肉类。3861与3516套件之间的UART串口互联可以保证检测结果在数秒内从3516套件输送到3861套件。

 