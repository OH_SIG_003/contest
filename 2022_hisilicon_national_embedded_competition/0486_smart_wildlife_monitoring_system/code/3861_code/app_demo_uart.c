/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <hi_stdlib.h>
#include <hisignalling_protocol.h>
#include <hi_uart.h>
#include <app_demo_uart.h>
#include <iot_uart.h>
#include <hi_gpio.h>
#include <hi_io.h>
#include "iot_i2c.h"
#include "iot_gpio_ex.h"
#include "ohos_init.h"
#include "iot_gpio.h"
#include "cmsis_os2.h"
#include "oled_ssd1306.h"
#include "oled_fonts.h"


#define LED_REDLI_GPIO 7   //红外传感器
#define LED_TEST_GPIO 9
#define LED_INTERVAL_TIME_US 100000
#define AHT20_BAUDRATE (400 * 1000)
#define AHT20_I2C_IDX 0


UartDefConfig uartDefConfig = {0};

static void Uart1GpioCOnfig(void)    //定义哪个板子
{
#ifdef ROBOT_BOARD
    IoSetFunc(HI_IO_NAME_GPIO_5, IOT_IO_FUNC_GPIO_5_UART1_RXD);
    IoSetFunc(HI_IO_NAME_GPIO_6, IOT_IO_FUNC_GPIO_6_UART1_TXD);
    /* IOT_BOARD */
#elif defined (EXPANSION_BOARD)
    IoSetFunc(HI_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoSetFunc(HI_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);
#endif
}

int SetUartRecvFlag(UartRecvDef def)   //标志位
{
    if (def == UART_RECV_TRUE) {
        uartDefConfig.g_uartReceiveFlag = HI_TRUE;
    } else {
        uartDefConfig.g_uartReceiveFlag = HI_FALSE;
    }
    
    return uartDefConfig.g_uartReceiveFlag;
}

int GetUartConfig(UartDefType type)    //串口配置
{
    int receive = 0;

    switch (type) {
        case UART_RECEIVE_FLAG:
            receive = uartDefConfig.g_uartReceiveFlag;
            break;
        case UART_RECVIVE_LEN:
            receive = uartDefConfig.g_uartLen;
            break;
        default:
            break;
    }
    return receive;
}

void ResetUartReceiveMsg(void)   //重置串口接收到的数据
{
    (void)memset_s(uartDefConfig.g_receiveUartBuff, sizeof(uartDefConfig.g_receiveUartBuff),
        0x0, sizeof(uartDefConfig.g_receiveUartBuff));
}

unsigned char *GetUartReceiveMsg(void)    //获取串口接收的数据
{
    return uartDefConfig.g_receiveUartBuff;
}


static hi_void *UartDemoTask(char *param)
{
    //int buff_read_size = 4;
    uint8_t uartBuff[UART_BUFF_SIZE] = { 0 };
    uint8_t *uart_buff_ptr = uartBuff;
    //hi_u8 uartBuff[4] = {0};
    hi_unref_param(param);
    printf("Initialize uart demo successfully, please enter some datas via DEMO_UART_NUM port...\n");
    Uart1GpioCOnfig();
    for (;;) {
        IoTUartRead(DEMO_UART_NUM, uart_buff_ptr, UART_BUFF_SIZE);
        //IoTUartRead(DEMO_UART_NUM, uartBuff, 4);
        //if ((uartDefConfig.g_uartLen > 0) && (uartBuff[0] == 0xaa) && (uartBuff[1] == 0x55)) {
            for (int i = 0; i < 4; i++) {
                printf("Uart1 read data%d:%c\n", i,uartBuff[i]);
                
            }
            TaskMsleep(200);
            printf("\r\n");
    //IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
    //usleep(LED_INTERVAL_TIME_US);
    // IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
    // usleep(LED_INTERVAL_TIME_US);
        TaskMsleep(20); /* 20:sleep 20ms */
    }
    return HI_NULL;
}

static const char *data = "0xfe";
static const char *data1 = "0xff";

// static char data = 0x01;
// static char data1 = 0x02;

char *flag = "none";

IotGpioValue val={0};

static void Uart_TX(void)
{

    uint8_t uartBuff[UART_BUFF_SIZE] = { 0 };
    uint8_t *uart_buff_ptr = uartBuff;
    uint8_t ret;
    IoTGpioInit(LED_REDLI_GPIO);   //初始化红外串口
    IoTGpioSetDir(LED_REDLI_GPIO, 0); //设置为输入模式
    Uart1GpioCOnfig(); //配置串口IO口
    printf("Initialize uart demo successfully, please enter some datas via DEMO_UART_NUM port...\n");
    while(1)
    {
        IoTGpioGetInputVal(LED_REDLI_GPIO,&val);
        if(val==IOT_GPIO_VALUE0)  //有遮挡，亮两个灯 (unsigned char *)
        {
            IoTUartWrite(DEMO_UART_NUM, (unsigned char *)data, strlen(data));
            IoTUartRead(DEMO_UART_NUM, uart_buff_ptr, UART_BUFF_SIZE);
            printf("uartBuff[3]%c\n", uartBuff[3]);
            //printf("Uart1 read data:%s\n", uart_buff_ptr);
            TaskMsleep(200);
        }
        else                    //值为1 无遮挡 亮一个灯
        {
            IoTUartWrite(DEMO_UART_NUM, (unsigned char *)data1, strlen(data));
            IoTUartRead(DEMO_UART_NUM, uart_buff_ptr, UART_BUFF_SIZE);
            printf("uartBuff[3]%c\n", uartBuff[3]);
            //printf("Uart1 read data:%s\n", uart_buff_ptr);
            TaskMsleep(200);
        }
        if(uartBuff[3]!=0)
        {
            IoTI2cInit(AHT20_I2C_IDX, AHT20_BAUDRATE);
        }
        switch (uartBuff[3])
        {
        case '1':
            OledShowString(20, 3, "buffalo ", 1); /* 屏幕第20列3行显示1行 */
            IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
            usleep(LED_INTERVAL_TIME_US);
            IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
            usleep(LED_INTERVAL_TIME_US);
            break;
        case '2':
            OledShowString(20, 3, "elephant", 1); /* 屏幕第20列3行显示1行 */
            IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
            usleep(LED_INTERVAL_TIME_US);
            IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
            usleep(LED_INTERVAL_TIME_US);
            break;
        case '4':
            OledShowString(20, 3, "rhino   ", 1); /* 屏幕第20列3行显示1行 */
            IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
            usleep(LED_INTERVAL_TIME_US);
            IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
            usleep(LED_INTERVAL_TIME_US);
            break;
        case '8':
            OledShowString(20, 3, "zebra   ", 1); /* 屏幕第20列3行显示1行 */
            IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
            usleep(LED_INTERVAL_TIME_US);
            IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
            usleep(LED_INTERVAL_TIME_US);
            break;
        
        default:
            OledShowString(20, 3, "error   ", 1); /* 屏幕第20列3行显示1行 */
            break;
        }
        
    }
}

/*
 * This demo simply shows how to read datas from UART2 port and then echo back.
 */
hi_void UartTransmit(hi_void)
{
    hi_u32 ret = 0;

    IotUartAttribute uartAttr = {
        .baudRate = 115200, /* baudRate: 115200 */
        .dataBits = 8, /* dataBits: 8bits */
        .stopBits = 1, /* stop bit */
        .parity = 0,
    };
    /* Initialize uart driver */
    //ret = IoTUartInit(DEMO_UART_NUM, &uartAttr);
    ret = IoTUartInit(DEMO_UART_NUM, &uartAttr);
    if (ret != HI_ERR_SUCCESS) {
        printf("Failed to init uart! Err code = %d\n", ret);
        return;
    }
    /* Create a task to handle uart communication */
    osThreadAttr_t attr = {0};
    attr.stack_size = UART_DEMO_TASK_STAK_SIZE;
    attr.priority = UART_DEMO_TASK_PRIORITY;
    attr.name = (hi_char*)"uart demo";
    OledInit();
    OledFillScreen(0);
    if (osThreadNew((osThreadFunc_t)Uart_TX, NULL, &attr) == NULL) {
        printf("Falied to create uartTX demo task!\n");
    }
}
SYS_RUN(UartTransmit);