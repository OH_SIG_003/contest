#ifndef SERVO_H
#define SERVO_H

#define HI_GPIO_SERVO 10 // Servo4,P6,10 Servo3,P5,9

void Servo_Send_PWM(int duty);
void *Servo_Control(void *param);

#endif