#ifndef __COMMON_MIPI_H__
#define __COMMON_MIPI_H__

#include "hi_types.h"
#include "hi_mipi.h"

HI_S32 SNS_MIPI_Init(combo_dev_t MipiDev);

HI_VOID SNS_MIPI_DeInit(combo_dev_t MipiDev);

HI_S32 LCD_MIPI_Init();

HI_VOID LCD_MIPI_DeInit();

#endif