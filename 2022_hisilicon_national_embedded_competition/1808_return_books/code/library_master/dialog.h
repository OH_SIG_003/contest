#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QDebug>
#include <QString>
#include <QList>
extern QFont font1,font2;
namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    QString picturePath;
    int paintEnable=0;
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
    void setID(QString id);
    void setName(QString name);
    void setMajor(QString major);
    void setGrade(QString grade);
    void setPicture(QString picturePath);
    void setBookList(QList<QString> list);
    //void paintEvent(QPaintEvent*);
private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
