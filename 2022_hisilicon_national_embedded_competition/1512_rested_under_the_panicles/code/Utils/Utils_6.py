def get_data_info(dir_path):    # 统计
    size = 0 # 存储大小
    Total = 0 # 图片数量
    bad_number = 0 # 破损数
    for root, dirs, files in os.walk(dir_path):
        img_files = [file_name for file_name in files if is_image(file_name)]
        files_size = sum([os.path.getsize(os.path.join(root, file_name)) for file_name in img_files])
        files_number = len(img_files)
        size += files_size
        Total += files_number
        for file in img_files:
            try:
                img = Image.open(os.path.join(root, file))
                img.load()
            except OSError:
                bad_number += 1
return size / 1024 / 1024, Total, bad_number  # size转成MB
