#ifndef YOLOV2_FALL_DETECT_H
#define YOLOV2_FALL_DETECT_H

#include "hi_type.h"

#if __cplusplus
extern "C" {
#endif

HI_S32 FallDetectInit();
HI_S32 FallDetectExit();
HI_S32 FallDetectCal(IVE_IMAGE_S *srcYuv, DetectObjInfo resArr[]);

#ifdef __cplusplus
}
#endif
#endif
