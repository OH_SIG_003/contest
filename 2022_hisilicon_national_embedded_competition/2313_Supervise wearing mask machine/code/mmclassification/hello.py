#rename photo 
import os
def myrename(path):
    file_list=os.listdir(path)
    for i,fi in enumerate(file_list):
        old_dir=os.path.join(path,fi)
        #rename
        if i>=0 and i<9:
            filename="00_error_mask_0000"+str(i+1)+"."+str(fi.split(".")[-1])
        elif i>=9 and i<99:
            filename="00_error_mask_000"+str(i+1)+"."+str(fi.split(".")[-1])
        elif i>=99 and i<999:
            filename="00_error_mask_00"+str(i+1)+"."+str(fi.split(".")[-1])
        else :
            filename="00_error_mask_0"+str(i+1)+"."+str(fi.split(".")[-1])
        new_dir=os.path.join(path,filename)
        try:
            os.rename(old_dir,new_dir)
        except Exception as e:
            print(e)
            print("Failed!")
        else:
            print("Success!")

if __name__=="__main__":
    #the photo way
    path="/home/mask"
    myrename(path)