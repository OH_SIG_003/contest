/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ssd1306_oled.h"
#include "c081_nfc.h"
#include "iot_i2c.h"
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include "app_demo_config.h"

//uart_add

#include <hi_stdlib.h>
#include <hisignalling_protocol.h>
#include <hi_uart.h>
#include <app_demo_uart.h>
#include <iot_uart.h>
#include <hi_gpio.h>
#include <hi_io.h>

//uart_end



unsigned char readReg = 0;
#define SEND_CMD_LEN (2)
#define NFC_TASK_SLEEP_1MS     (1)

/* i2c read */
unsigned int WriteRead(unsigned char regHigh8bitCmd, unsigned char regLow8bitCmd,
    unsigned char* recvData, unsigned char sendLen, unsigned char readLen)
{
    IotI2cData co8iNfcI2cReadData = {0};
    IotI2cData c081NfcI2cWriteCmdAddr = {0};

    unsigned char sendUserCmd[SEND_CMD_LEN] = {regHigh8bitCmd, regLow8bitCmd};
    (void)memset_s(&co8iNfcI2cReadData, sizeof(IotI2cData), 0x0, sizeof(IotI2cData));

    c081NfcI2cWriteCmdAddr.sendBuf = sendUserCmd;
    c081NfcI2cWriteCmdAddr.sendLen = sendLen;
    co8iNfcI2cReadData.receiveBuf = recvData;
    co8iNfcI2cReadData.receiveLen = readLen;

    readReg = NFC_CLEAN; // 消除stop信号

    IoTI2cWrite(IOT_I2C_IDX_0, C081_NFC_ADDR & 0xFE,
                c081NfcI2cWriteCmdAddr.sendBuf, c081NfcI2cWriteCmdAddr.sendLen);

    IoTI2cRead(IOT_I2C_IDX_0, C081_NFC_ADDR | I2C_RD,
        co8iNfcI2cReadData.receiveBuf, co8iNfcI2cReadData.receiveLen);
    return 0;
}

#define EEPROM_CMD_1    (0x3B1)
#define EEPROM_CMD_2    (0x3B5)
#define SEND_EEPROM_CMD_LEN (1)
#define SEND_EEPROM_DATA_1  (1)
#define SEND_EEPROM_DATA_2  (3)

/* NFC 芯片配置 ,平时不要调用 NFC init */
void NfcInit(void)
{
    // uint8_t wbuf[5]={0x05,0x72,0xF7,0x60,0x02}; // 芯片默认配置
    unsigned char wBuf[5] = {0x05, 0x78, 0xF7, 0x90, 0x02}; // 芯片默认配置
    /* 读取字节的时候屏蔽csn引脚,写eep的时候打开 */
    IoSetFunc(IOT_IO_NAME_GPIO9, IOT_IO_FUNC_GPIO_9_GPIO);
    IoTGpioSetDir(IOT_GPIO_IDX_9, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(IOT_IO_NAME_GPIO9, IOT_GPIO_VALUE0);

    Fm11WriteEep(EEPROM_CMD_1, SEND_EEPROM_CMD_LEN, &wBuf[SEND_EEPROM_DATA_1]); /* send EEPROM cmd */
    Fm11WriteEep(EEPROM_CMD_2, SEND_EEPROM_CMD_LEN, &wBuf[SEND_EEPROM_DATA_2]); /* send EEPROM cmd */
}

void *NfcTask(const char* param)
{
    (void) param;
    IoTGpioInit(IOT_IO_NAME_GPIO_13);
    IoSetFunc(IOT_IO_NAME_GPIO_13, IOT_IO_FUNC_GPIO_13_I2C0_SDA);
    IoTGpioInit(IOT_IO_NAME_GPIO_14);
    IoSetFunc(IOT_IO_NAME_GPIO_14, IOT_IO_FUNC_GPIO_14_I2C0_SCL);

    IoTI2cInit(IOT_I2C_IDX_0, HI_I2C_IDX_BAUDRATE); // baud 400k
    IoTI2cSetBaudrate(IOT_I2C_IDX_0, HI_I2C_IDX_BAUDRATE);
    printf("nfc task\r\n");
    NfcRead();
}

/* c08i nfc task */
void NfcExampleEntry(void)
{
    osThreadAttr_t attr = {0};

    attr.stack_size = C08I_NFC_DEMO_TASK_STAK_SIZE;
    attr.priority = C08I_NFC_TASK_PRIORITY;
    attr.name = "nfcTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;

    if (osThreadNew((osThreadFunc_t)NfcTask, NULL, &attr) == NULL) {
        printf("[nfcExample] Falied to create LedTask!\n");
    }
}

SYS_RUN(NfcExampleEntry);

/* nfc display */
void *AppNfcDisplay(char* param)
{
    for (; ;) {
        OledNfcDisplay();
        TaskMsleep(NFC_TASK_SLEEP_1MS);
    }
}

/* nfc display task */
void NfcDisplayExampleEntry(void)
{
    osThreadAttr_t attr = {0};
    attr.stack_size = NFC_DISPLAY_TASK_STAK_SIZE;
    attr.priority = C08I_NFC_DEMO_TASK_PRIORITY;
    attr.name = "app_nfc_display";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    if (osThreadNew((osThreadFunc_t)AppNfcDisplay, NULL, &attr) == NULL) {
        printf("[nfcDisplayExampleEntry] Falied to create LedTask!\n");
    }
}

SYS_RUN(NfcDisplayExampleEntry);

//add

#include <hi_task.h>
#include <string.h>
#include <hi_wifi_api.h>
#include <hi_mux.h>
#include <hi_io.h>
#include <hi_gpio.h>
#include "iot_config.h"
#include "iot_log.h"
#include "iot_main.h"
#include "iot_profile.h"
#include "cmsis_os2.h"

/* attribute initiative to report */
#define TAKE_THE_INITIATIVE_TO_REPORT
#define ONE_SECOND                          (1000)
/* oc request id */
#define CN_COMMADN_INDEX                    "commands/request_id="
#define WECHAT_SUBSCRIBE_LIGHT              "light"
#define WECHAT_SUBSCRIBE_LIGHT_ON_STATE     "1"
#define WECHAT_SUBSCRIBE_LIGHT_OFF_STATE    "0"

#define WECHAT_SUBSCRIBE_UART              "uart"
#define WECHAT_SUBSCRIBE_UART_ON_STATE     "1"
#define WECHAT_SUBSCRIBE_UART_OFF_STATE    "0"

int g_ligthStatus = -1;
int g_uartStatus = -1;
typedef void (*FnMsgCallBack)(hi_gpio_value val);

typedef struct FunctionCallback {
    hi_bool  stop;
    hi_u32 conLost;
    hi_u32 queueID;
    hi_u32 iotTaskID;
    FnMsgCallBack    msgCallBack;
}FunctionCallback;
FunctionCallback g_functinoCallback;

/* CPU Sleep time Set */
/*
unsigned int TaskMsleep(unsigned int ms)            //重复定义app_demo_i2c_oled.c和hal_iot_gpio_ex.c
{
    if (ms <= 0) {
        return HI_ERR_FAILURE;
    }
    return hi_sleep((hi_u32)ms);
}
*/

static void DeviceConfigInit(hi_gpio_value val)
{
    hi_io_set_func(HI_IO_NAME_GPIO_9, HI_IO_FUNC_GPIO_9_GPIO);
    hi_gpio_set_dir(HI_GPIO_IDX_9, HI_GPIO_DIR_OUT);
    hi_gpio_set_ouput_val(HI_GPIO_IDX_9, val);
}

static int  DeviceMsgCallback(FnMsgCallBack msgCallBack)
{
    g_functinoCallback.msgCallBack = msgCallBack;
    return 0;
}

static void wechatControlDeviceMsg(hi_gpio_value val)
{
    DeviceConfigInit(val);
}

unsigned int inter_state = 1;
// < this is the callback function, set to the mqtt, and if any messages come, it will be called
// < The payload here is the json string
static void DemoMsgRcvCallBack(int qos, const char *topic, const char *payload)
{
    IOT_LOG_DEBUG("RCVMSG:QOS:%d TOPIC:%s PAYLOAD:%s\r\n", qos, topic, payload);

    /* 云端下发命令后，板端的操作处理 */
    if (strstr(payload, WECHAT_SUBSCRIBE_LIGHT) != NULL) {
        if (strstr(payload, WECHAT_SUBSCRIBE_LIGHT_OFF_STATE) != NULL) {
            wechatControlDeviceMsg(HI_GPIO_VALUE1);
            g_ligthStatus = HI_FALSE;
        } else {
            wechatControlDeviceMsg(HI_GPIO_VALUE0);
            g_ligthStatus = HI_TRUE;
        }
    }
    if (strstr(payload, WECHAT_SUBSCRIBE_UART) != NULL) {
        if (strstr(payload, WECHAT_SUBSCRIBE_UART_OFF_STATE) != NULL) {
            inter_state = 1;
            printf("inter_lock\n");
            g_ligthStatus = HI_FALSE;
        } else {
            inter_state = 0;
            printf("inter_unlock\n");
            g_ligthStatus = HI_TRUE;
        }
    }
    return HI_NULL;
}

/* publish sample */
hi_void IotPublishSample(void)
{
    /* reported attribute */
    WeChatProfile weChatProfile = {
        .subscribeType = "type",
        .status.subState = "state",
        .status.subReport = "reported",
        .status.reportVersion = "version",
        .status.Token = "clientToken",
        /* report motor */
        .reportAction.subDeviceActionuart = "uart",
        .reportAction.motorActionStatus = 0, /* 0 : motor off */
        /* report humidity */
        .reportAction.subDeviceActionHumidity = "humidity",
        .reportAction.humidityActionData = 70, /* humidity data */
        /* report light_intensity */
        .reportAction.subDeviceActionLightIntensity = "light_intensity",
        .reportAction.lightIntensityActionData = 60, /* 60 : light_intensity */
    };

    /* report light */
    if (g_ligthStatus == HI_TRUE) {
        weChatProfile.reportAction.subDeviceActionLight = "light";
        weChatProfile.reportAction.lightActionStatus = 1; /* 1: light on */
    } else if (g_ligthStatus == HI_FALSE) {
        weChatProfile.reportAction.subDeviceActionLight = "light";
        weChatProfile.reportAction.lightActionStatus = 0; /* 0: light off */
    } else {
        weChatProfile.reportAction.subDeviceActionLight = "light";
        weChatProfile.reportAction.lightActionStatus = 0; /* 0: light off */
    }
    
    //
        /* report light */
    if (g_uartStatus == HI_TRUE) {
        weChatProfile.reportAction.subDeviceActionuart = "uart";
        weChatProfile.reportAction.uartActionStatus = 1; /* 1: light on */
    } else if (g_ligthStatus == HI_FALSE) {
        weChatProfile.reportAction.subDeviceActionuart = "uart";
        weChatProfile.reportAction.uartActionStatus = 0; /* 0: light off */
    } else {
        weChatProfile.reportAction.subDeviceActionuart = "uart";
        weChatProfile.reportAction.uartActionStatus = 0; /* 0: light off */
    }

    /* profile report */
    IoTProfilePropertyReport(CONFIG_USER_ID, &weChatProfile);
}

// < this is the demo main task entry,here we will set the wifi/cjson/mqtt ready and
// < wait if any work to do in the while
extern unsigned int nfc_state;
extern unsigned int cloud_state;
static hi_void *DemoEntry(const char *arg)
{
    WifiStaReadyWait();
    cJsonInit();
    IoTMain();
    /* 云端下发回调 */
    IoTSetMsgCallback(DemoMsgRcvCallBack);
    /* 主动上报 */
#ifdef TAKE_THE_INITIATIVE_TO_REPORT
    while (1) {
        /* 用户可以在这调用发布函数进行发布，需要用户自己写调用函数 */
        if ((nfc_state == 2)&&(cloud_state == 1))
        {
            IotPublishSample(); // 发布例程 
        }  
#endif
        TaskMsleep(ONE_SECOND);
    }
    return NULL;
}


// < This is the demo entry, we create a task here,
// and all the works has been done in the demo_entry
#define CN_IOT_TASK_STACKSIZE  0x1000
#define CN_IOT_TASK_PRIOR 25
#define CN_IOT_TASK_NAME "IOTDEMO"

static void AppDemoIot(void)
{
    osThreadAttr_t attr;
    IoTWatchDogDisable();

    attr.name = "IOTDEMO";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = CN_IOT_TASK_STACKSIZE;
    attr.priority = CN_IOT_TASK_PRIOR;

    if (osThreadNew((osThreadFunc_t)DemoEntry, NULL, &attr) == NULL) {
        printf("[mqtt] Falied to create IOTDEMO!\n");
    }
}

SYS_RUN(AppDemoIot);

//add end



////uart_add

UartDefConfig uartDefConfig = {0};

static void Uart1GpioCOnfig(void)
{
#ifdef ROBOT_BOARD
    IoSetFunc(HI_IO_NAME_GPIO_5, IOT_IO_FUNC_GPIO_5_UART1_RXD);
    IoSetFunc(HI_IO_NAME_GPIO_6, IOT_IO_FUNC_GPIO_6_UART1_TXD);
    /* IOT_BOARD */
#elif defined (EXPANSION_BOARD)
    IoSetFunc(HI_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoSetFunc(HI_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);
#endif
}

int SetUartRecvFlag(UartRecvDef def)
{
    if (def == UART_RECV_TRUE) {
        uartDefConfig.g_uartReceiveFlag = HI_TRUE;
    } else {
        uartDefConfig.g_uartReceiveFlag = HI_FALSE;
    }
    
    return uartDefConfig.g_uartReceiveFlag;
}

int GetUartConfig(UartDefType type)
{
    int receive = 0;

    switch (type) {
        case UART_RECEIVE_FLAG:
            receive = uartDefConfig.g_uartReceiveFlag;
            break;
        case UART_RECVIVE_LEN:
            receive = uartDefConfig.g_uartLen;
            break;
        default:
            break;
    }
    return receive;
}

void ResetUartReceiveMsg(void)
{
    (void)memset_s(uartDefConfig.g_receiveUartBuff, sizeof(uartDefConfig.g_receiveUartBuff),
        0x0, sizeof(uartDefConfig.g_receiveUartBuff));
}

unsigned char *GetUartReceiveMsg(void)
{
    return uartDefConfig.g_receiveUartBuff;
}


static hi_void *UartDemoTask(char *param)
{
    extern unsigned int inter_state;
    hi_u8 uartBuff[UART_BUFF_SIZE] = {0};
    hi_unref_param(param);
    printf("Initialize uart demo successfully, please enter some datas via DEMO_UART_NUM port...\n");
    Uart1GpioCOnfig();
    for (;;) {
        uartDefConfig.g_uartLen = IoTUartRead(DEMO_UART_NUM, uartBuff, UART_BUFF_SIZE);
        if ((uartDefConfig.g_uartLen > 0) && (uartBuff[0] == 0xaa) && (uartBuff[1] == 0x55)) {
            if (inter_state == 1)
            {
                printf("start_uart\n");
                if (GetUartConfig(UART_RECEIVE_FLAG) == HI_FALSE) 
                {
                    (void)memcpy_s(uartDefConfig.g_receiveUartBuff, uartDefConfig.g_uartLen,
                                    uartBuff, uartDefConfig.g_uartLen);
                    (void)SetUartRecvFlag(UART_RECV_TRUE);
                }
            }

            //here?
            printf("here\n");
            printf("%s",uartBuff);
            
        }
        TaskMsleep(20); /* 20:sleep 20ms */
    }
    return HI_NULL;
}

/*
 * This demo simply shows how to read datas from UART2 port and then echo back.
 */
hi_void UartTransmit(hi_void)
{
    hi_u32 ret = 0;

    IotUartAttribute uartAttr = {
        .baudRate = 115200, /* baudRate: 115200 */
        .dataBits = 8, /* dataBits: 8bits */
        .stopBits = 1, /* stop bit */
        .parity = 0,
    };

    /* Initialize uart driver */
    ret = IoTUartInit(DEMO_UART_NUM, &uartAttr);
    if (ret != HI_ERR_SUCCESS) {
        printf("Failed to init uart! Err code = %d\n", ret);
        return;
    }

    /* Create a task to handle uart communication */
    osThreadAttr_t attr = {0};
    attr.stack_size = UART_DEMO_TASK_STAK_SIZE;
    attr.priority = UART_DEMO_TASK_PRIORITY;
    attr.name = (hi_char*)"uart demo";
    if (osThreadNew((osThreadFunc_t)UartDemoTask, NULL, &attr) == NULL) {
        printf("Falied to create uart demo task!\n");
    }
}

SYS_RUN(UartTransmit);

//uart_add_end