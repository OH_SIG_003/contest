
#ifndef __ROBOT_MAIN_H__
#define __ROBOT_MAIN_H__
#include "stdint.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <hi_task.h>
#include "scs15_control.h"
#include "communication_ros.h"
#include "hi_io.h"
#include "cmsis_os2.h"

#include "adc_voltage.h"

#include <hi_gpio.h>
void robot_init(void);
hi_u32 robot_control_task_init(hi_void);
#endif /*__ROBOT_MAIN_H__ */

