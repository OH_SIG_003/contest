from MainWindow import Ui_MainWindow
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import PyQt5.QtCore as QtCore
from ConfigFile import roadPointList, reference_size


class ImageProcessor(object):
    def __init__(self, pixmap):
        # self.pixmap = pixmap
        self.__canvasSize = pixmap.size()
        self.__painter = QPainter(pixmap)
        self.__brush = QBrush()
        self.__brush.setStyle(Qt.SolidPattern)
        self.__polygonList = []
        self.__colorList = []
        self.__polygonInit()
        self.__colorInit()

    def __polygonInit(self):
        w, h = self.__canvasSize.width(), self.__canvasSize.height()
        # 顺序为：北南东西，先1后2
        self.__polygonList.append(QPolygon([QPoint(w/2, 0), QPoint(w*3/5, 0), QPoint(w*3/5, h*2/5), QPoint(w/2, h/2)])) # 北1
        self.__polygonList.append(QPolygon([QPoint(w*2/5, 0), QPoint(w/2, 0), QPoint(w/2, h/2), QPoint(w*2/5, h*2/5)])) # 北2
        self.__polygonList.append(QPolygon([QPoint(w*2/5, h), QPoint(w/2, h), QPoint(w/2, h/2), QPoint(w*2/5, h*3/5)]))
        self.__polygonList.append(QPolygon([QPoint(w/2, h), QPoint(w*3/5, h), QPoint(w*3/5, h*3/5), QPoint(w/2, h/2)]))
        self.__polygonList.append(QPolygon([QPoint(w, h*3/5), QPoint(w, h/2), QPoint(w/2, h/2), QPoint(w*3/5, h*3/5)]))
        self.__polygonList.append(QPolygon([QPoint(w, h*2/5), QPoint(w, h/2), QPoint(w/2, h/2), QPoint(w*3/5, h*2/5)]))
        self.__polygonList.append(QPolygon([QPoint(0, h*2/5), QPoint(0, h/2), QPoint(w/2, h/2), QPoint(w*2/5, h*2/5)]))
        self.__polygonList.append(QPolygon([QPoint(0, h/2), QPoint(0, h*3/5), QPoint(w*2/5, h*3/5), QPoint(w/2, h/2)]))

    def __colorInit(self):
        self.__colorList.append(QColor(29, 189, 123))
        self.__colorList.append(QColor(253, 162, 40))
        self.__colorList.append(QColor(252, 49, 46))

    def paintImg(self, congestionList):
        for i in range(len(self.__polygonList)):
            self.__brush.setColor(self.__colorList[int(congestionList[i])])
            self.__painter.setBrush(self.__brush)
            self.__painter.drawPolygon(self.__polygonList[i])


class RoadImageProcessor(object):
    def __init__(self, painter, offset, size):
        self.__painter = painter
        self.__offset = [offset.x(), offset.y()]
        self.__w, self.__h = size.width(), size.height()
        self.__newRoadPointList = [[[0, 0], [0, 0]] for _ in range(len(roadPointList))]
        self.__colorList = []
        self.__pointInit()
        self.__colorInit()
        self.__size = [2,2,2,3,4,5]
        self.__sizeIndex = 0

    def __pointInit(self):
        for i in range(len(self.__newRoadPointList)):
            for j in range(len(self.__newRoadPointList[i])):
                for k in range(len(self.__newRoadPointList[i][j])):
                    self.__newRoadPointList[i][j][k] = self.__offset[k] + int(roadPointList[i][j][k] * self.__w / reference_size)

    def __colorInit(self):
        self.__colorList.append(QColor(29, 189, 123))
        self.__colorList.append(QColor(253, 162, 40))
        self.__colorList.append(QColor(252, 49, 46))

    def paintRoad(self, congestionList, roadWidth):
        for i in range(len(congestionList)):
            self.__painter.setPen(QPen(self.__colorList[congestionList[i]], roadWidth))
            self.__painter.drawLine(QPoint(self.__newRoadPointList[i][0][0], self.__newRoadPointList[i][0][1]),
                                    QPoint(self.__newRoadPointList[i][1][0], self.__newRoadPointList[i][1][1]))

    def roadRescale(self, flag):
        if flag == 1: # 放大
            self.__sizeIndex += 1
        else:
            self.__sizeIndex -= 1

if __name__ == '__main__':
    pass