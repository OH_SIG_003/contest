
#ifndef MY_CONFIG_H
#define MY_CONFIG_H

#define MY_CODE_H

#define YOLOV3_H
// #define YOLOV2_H

#define MY_RESIZE_WIDTH 960
#define MY_RESIZE_HEIGHT 608

#define YOLOV2_WIDTH 416
#define YOLOV2_HEIGHT 416

#define YOLOV3_WIDTH 608
#define YOLOV3_HEIGHT 608

#define MODEL_FILE_YOLOV2 "/userdata/my_models/yolov2-background.wk"
#define MODEL_FILE_YOLOV3 "/userdata/my_models/yolov3-car.wk"

#endif