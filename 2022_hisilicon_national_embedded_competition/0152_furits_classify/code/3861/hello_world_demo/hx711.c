/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <unistd.h>
#include <hi_time.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "led_example.h"
#include "iot_gpio_ex.h"

#define LED_INTERVAL_TIME_US 300000
#define LED_TASK_STACK_SIZE 4096
#define LED_TASK_PRIO 28

#define Gapvalue 430;
unsigned long Read(unsigned int GPIO_0,unsigned int GPIO_1);
double get_base(unsigned int GPIO_0,unsigned int GPIO_1);
double weight_show;
double weight;
double weight1;
double weight11;
double weight22;
double weight2;
double weights2;
double weights1;
double base2;
double base1;
double base;
int f,b;
f=0;
b=0;



    void *SCALE2(const char *arg)
{
    (void)arg;
    base=get_base(IOT_IO_NAME_GPIO_11,IOT_IO_NAME_GPIO_12);
    base1=get_base(IOT_IO_NAME_GPIO_9,IOT_IO_NAME_GPIO_10);
    base2=get_base(IOT_IO_NAME_GPIO_7,IOT_IO_NAME_GPIO_2);
    
    while(1){
        weight=(Read(IOT_IO_NAME_GPIO_11,IOT_IO_NAME_GPIO_12)-base)/Gapvalue;
        if(weight>0){     
        printf("weight = %0.f \n",weight);
        weight_show=weight;
        }
        hi_sleep(1000);

        weight1=(Read(IOT_IO_NAME_GPIO_9,IOT_IO_NAME_GPIO_10)-base1)/Gapvalue;
        if(weight1>50){     
        printf("weight1 = %0.f \n",weight1);
        f++;

        if(weight1>50 && f==1){
            
            weight11=weight1;
             
            printf("weight11 = %0.f \n",weight11);

        }

        }
        hi_sleep(1000);

        weight2=(Read(IOT_IO_NAME_GPIO_7,IOT_IO_NAME_GPIO_2)-base2)/Gapvalue;
        if(weight2>5){     
        printf("weight2 = %0.f \n",weight2);
        b++;
        if(weight2>5 && b==1){
            weight22=weight2;
            printf("weight22 = %0.f \n",weight22);

        }

        weights1=weight11-weight1;
        printf("%0.f \n",weights1);
        if (weights1<0){
            weights1=0;
        }
        printf("%0.f \n",weights1);
        weights2=weight22-weight2;
        if (weights2<0){
            weights2=0;
        }
        printf("%0.f \n",weights2);
        }
        hi_sleep(1000);
    } 
    return NULL;
}
unsigned long Read(unsigned int GPIO_0,unsigned int GPIO_1)
{
	unsigned long value = 0;
	unsigned char i = 0;
    IotGpioValue input = 0;
	hi_udelay(2);
	//时钟线拉低 空闲时时钟线保持低电位
	IoTGpioSetOutputVal(GPIO_1,0);
	hi_udelay(2);	
	IoTGpioGetInputVal(GPIO_0,&input);
	//等待AD转换结束
	while(input)
    {
        IoTGpioGetInputVal(GPIO_0,&input);
    }
	for(i=0;i<24;i++)
	{
		//时钟线拉高 开始发送时钟脉冲
		IoTGpioSetOutputVal(GPIO_1,1);
		hi_udelay(2);
		//左移位 右侧补零 等待接收数据
		value = value << 1;
		//时钟线拉低
		IoTGpioSetOutputVal(GPIO_1,0);
		hi_udelay(2);
		//读取一位数据
        IoTGpioGetInputVal(GPIO_0,&input);
		if(input){
			value ++;
        }
	}
	//第25个脉冲
	IoTGpioSetOutputVal(GPIO_1,1);
	hi_udelay(2);
	value = value^0x800000;	
	//第25个脉冲结束
	IoTGpioSetOutputVal(GPIO_1,0);	
	hi_udelay(2);	
	return value;
}

double get_base(unsigned int GPIO_0,unsigned int GPIO_1){

    double sum = 0;    // 为了减小误差，一次取出10个值后求平均值。
  	for (int i = 0; i < 10; i++) // 循环的越多精度越高，当然耗费的时间也越多
    	sum += Read(GPIO_0,GPIO_1);  // 累加
  	return (sum/10); // 求平均值进行均差

}

static void HX711ExampleEntry2(void)
{
    osThreadAttr_t attr;

    
    IoSetFunc(IOT_IO_NAME_GPIO_9,IOT_IO_FUNC_GPIO_9_GPIO);
    IoSetFunc(IOT_IO_NAME_GPIO_10,IOT_IO_FUNC_GPIO_10_GPIO);
   
    IoTGpioSetDir(IOT_IO_NAME_GPIO_9, IOT_GPIO_DIR_IN);
    IoTGpioSetDir(IOT_IO_NAME_GPIO_10, IOT_GPIO_DIR_OUT);
    IoSetFunc(IOT_IO_NAME_GPIO_11,IOT_IO_FUNC_GPIO_11_GPIO);
    IoSetFunc(IOT_IO_NAME_GPIO_12,IOT_IO_FUNC_GPIO_12_GPIO);
   
    IoTGpioSetDir(IOT_IO_NAME_GPIO_11, IOT_GPIO_DIR_IN);
    IoTGpioSetDir(IOT_IO_NAME_GPIO_12, IOT_GPIO_DIR_OUT);

	IoSetFunc(IOT_IO_NAME_GPIO_7,IOT_IO_FUNC_GPIO_7_GPIO);
    IoSetFunc(IOT_IO_NAME_GPIO_2,IOT_IO_FUNC_GPIO_2_GPIO);
   
    IoTGpioSetDir(IOT_IO_NAME_GPIO_7, IOT_GPIO_DIR_IN);
    IoTGpioSetDir(IOT_IO_NAME_GPIO_2, IOT_GPIO_DIR_OUT);


    

    attr.name = "SCALE2";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = LED_TASK_STACK_SIZE;
    attr.priority = LED_TASK_PRIO;

    if (osThreadNew((osThreadFunc_t)SCALE2, NULL, &attr) == NULL) {
        printf("[LedExample] Falied to create SCALE!\n");
    }
    return NULL;
}


SYS_RUN(HX711ExampleEntry2);