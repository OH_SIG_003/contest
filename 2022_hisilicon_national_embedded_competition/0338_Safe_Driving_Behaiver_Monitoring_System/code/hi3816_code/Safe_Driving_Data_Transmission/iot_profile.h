
#ifndef IOT_PROFILE_H
#define IOT_PROFILE_H

#include <hi_types_base.h>
#include "iot_config.h"

#define OC_BEEP_STATUS_ON       ((hi_u8) 0x01)
#define OC_BEEP_STATUS_OFF      ((hi_u8) 0x00)

// < enum all the data type for the oc profile
typedef enum {
    EN_IOT_DATATYPE_INT = 0,
    EN_IOT_DATATYPE_LONG,
    EN_IOT_DATATYPE_FLOAT,
    EN_IOT_DATATYPE_DOUBLE,
    EN_IOT_DATATYPE_STRING,           // < must be ended with '\0'
    EN_IOT_DATATYPE_LAST,
}IoTDataType_t;

typedef enum {
    OC_LED_ON = 1,
    OC_LED_OFF
}OcLedValue;

typedef struct {
    void                            *nxt; // < ponit to the next key
    const char                      *key;
    const char                      *value;
    hi_u32                          iValue;
    IoTDataType_t                   type;
}IoTProfileKV_t;

typedef struct {
    void *nxt;
    char *serviceID;
    char *eventTime;
    IoTProfileKV_t *serviceProperty;
}IoTProfileService_t;

typedef struct {
    int  retCode;           // < response code, 0 success while others failed
    const char   *respName; // < response name
    const char   *requestID; // < specified by the message command
    IoTProfileKV_t  *paras; // < the command paras
}IoTCmdResp_t;

typedef struct {
    const char *subState;
    const char *subReport;
    const char *reportVersion;
    const char *Token;
}WeChatProfileStatus;
//555
typedef struct {
    
    int heartActionStatus;
    int MQ3ActionData;
    int FSRActionData;
    int drinkActionData;
    int phoneActionData;
    int smokeActionData;
   // int heartActionData;
    const char *subDeviceActionheart;
    const char *subDeviceActionMQ3;
    const char *subDeviceActionFSR;
    const char *subDeviceActiondrink;
    const char *subDeviceActionphone;
    const char *subDeviceActionsmoke;
   // const char *subDeviceActionheart;
}WeChatProfileReporte;

typedef struct {
    const char *subscribeType;
    WeChatProfileStatus status;
    WeChatProfileReporte reportAction;
}WeChatProfile;

/**
 * use this function to report the property to the iot platform
*/
int IoTProfilePropertyReport(char *deviceID, WeChatProfile *payload);
void cJsonInit(void);
void WifiStaReadyWait(void);
#endif