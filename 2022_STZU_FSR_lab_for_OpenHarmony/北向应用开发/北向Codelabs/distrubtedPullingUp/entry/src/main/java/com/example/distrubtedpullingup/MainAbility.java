package com.example.distrubtedpullingup;

import ohos.ace.ability.AceAbility;
import ohos.aafwk.content.Intent;

public class MainAbility extends AceAbility {
    @Override
    public void onStart(Intent intent) {
        //获取分布式设备拉起权限
        String[] permissions = {"ohos.permission.DISTRIBUTED_DATASYNC"};
        requestPermissionsFromUser(permissions, 0);
        super.onStart(intent);
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
