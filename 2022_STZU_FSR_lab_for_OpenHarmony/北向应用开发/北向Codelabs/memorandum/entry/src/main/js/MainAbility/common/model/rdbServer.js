import featureAbility from '@ohos.ability.featureAbility'
import data_rdb from '@ohos.data.rdb'
const STORE_CONFIG = { name: "RdbTest.db" }
export default class rbdStoreModel {
    rdbStore
    #tableList = []
    constructor(SQL_CREATE_TABLE_LIST) { //---可插入多条SQL语句，一个库建多个表
        for (let i in SQL_CREATE_TABLE_LIST) {
            this.#tableList.push(SQL_CREATE_TABLE_LIST[i])
        }
    }
    createKvStore(callback) {
        if (typeof (this.rdbStore) === 'undefined') {
            let self = this;
            let context = featureAbility.getContext();
            let promise = data_rdb.getRdbStore(context, STORE_CONFIG, 1)
            promise.then((rdbStore) => {
                self.rdbStore = rdbStore;
                for (let i in this.#tableList) {
                    rdbStore.executeSql(this.#tableList[i], null);
                }
                console.info("xxx--- rdbStore " + 'create table.')
                callback();
            }).catch((err) => {
                console.info("xxx--- rdbStore" + err)
                callback();
            })
        } else {
            callback();
        }
    }
    insertValue(table, valueBucket) {
        this.createKvStore(() => {
            let promise = this.rdbStore.insert(table, valueBucket)
            promise.then((rows) => {
                console.info('xxx--- rdbStore.insert done ' + rows)
            })
        })
    }
    updateValue(valueBucket, table, field, value) {
        this.createKvStore(() => {
            console.info(`xxx--- rdbStore.update field = ${field} value = ${value} == ${JSON.stringify(valueBucket)}`)
            let predicates = new data_rdb.RdbPredicates(table)
            predicates.equalTo(field, value)
            this.rdbStore.update(valueBucket, predicates, function (err, rows) {
                console.info("xxx--- rdbStore.update updated row count: " + rows)
            })
        })
    }

    deleteValue(table, field, value,cb) {
        this.createKvStore(() => {
            console.info(`xxx--- rdbStore.delete field = ${field} value = ${value}`)
            let predicates = new data_rdb.RdbPredicates(table)
            predicates.equalTo(field, value)
            this.rdbStore.delete(predicates, (err, rows) => {
                if (rows === 0) {
                    console.info("xxx--- rdbStore.delete rows: -1")
                }
                else console.info("xxx--- rdbStore.delete rows: " + rows)
                cb()
            })
        })
    }

    queryValue(table,callback) {
        this.createKvStore(() => {
            let predicates = new data_rdb.RdbPredicates(table)
            let promise = this.rdbStore.query(predicates)
            console.info("xxx--- rdbStore query start")
            promise.then((resultSet) => {
                callback(resultSet);
            })
        })
    }

    search(table,field,value,callback){
        this.createKvStore(()=>{
            let predicates = new data_rdb.RdbPredicates(table)
            predicates.contains(field,value)
            let promise = this.rdbStore.query(predicates)
            console.info("xxx--- rdbStore search start")
            promise.then((resultSet) => {
                callback(resultSet);
            })
        })
    }
}