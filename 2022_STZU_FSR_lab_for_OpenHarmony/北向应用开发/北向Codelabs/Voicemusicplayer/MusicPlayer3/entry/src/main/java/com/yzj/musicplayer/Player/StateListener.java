package com.yzj.musicplayer.Player;

public interface StateListener {
    void onPlaySuccess(int totalTime);

    void onPauseSuccess();

    void onPositionChange(int currentTime);

    void onMusicFinished();

    void onUriSet(String name);
}
