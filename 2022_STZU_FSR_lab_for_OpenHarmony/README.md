# SZTU-FSR LAB for OpenHarmony

#### 简介
该仓库是用于OpenHarmony&HarmonyOS南北向系统学习资料整合（长期更新），由深圳技术大学开源创客协会以及FSR实验室学生团队负责。

仓库资料内容包括`技术博客`，`demo源码（Codelabs）`,`项目成果视频展示`等，

* ## 北向应用开发

  * ### [北向技术博客](./北向应用开发/北向技术博客)

  * ### [北向Codelabs](./北向应用开发/北向Codelabs)

* ## 南向设备开发

  * ### [南向技术博客](./南向设备开发/南向技术博客)

  * ### [南向Codelabs](./南向设备开发/南向Codelabs)

* ## 项目成果视频展示

  * [【FFH】基于鸿蒙操作系统的自动配药系统](https://www.bilibili.com/video/BV1kG41187mw?vd_source=8e332ca880683b4c6a61ea1dae5a8aed)

  * [【FFH】基于鸿蒙操作系统的游戏交互式少儿语言文化教育APP](https://www.bilibili.com/video/BV13Y4y1u7UC?spm_id_from=333.999.0.0)


* ## 贡献者介绍如下



