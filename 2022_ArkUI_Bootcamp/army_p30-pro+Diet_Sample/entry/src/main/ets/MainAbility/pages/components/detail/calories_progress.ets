import { FoodInfo } from '../../../model/type/DietType'
import { CategoryEnum } from '../../../model/enum/DietEnum'

import { CardTitle } from './card_title'

@Component
export struct CaloriesProgress {
    private foodInfo: FoodInfo
    private averageCalories: number = 0
    private totalCalories: number = 0
    private highCalories: boolean = false

    aboutToAppear() {
        switch (this.foodInfo.categoryId) {
            case CategoryEnum.Vegetable:
                this.averageCalories = 26
                break
            case CategoryEnum.Fruit:
                this.averageCalories = 60
                break
            case CategoryEnum.Nut:
                this.averageCalories = 606
                break
            case CategoryEnum.Seafood:
                this.averageCalories = 56
                break
            case CategoryEnum.Dessert:
                this.averageCalories = 365
                break
        }
        this.totalCalories = this.averageCalories * 2
        this.highCalories = this.foodInfo.calories < this.averageCalories
    }

    build() {
        Column() {
            CardTitle({ title: $r('app.string.diet_record_calorie'), subtitle: $r('app.string.unit_weight') })

            Row() {
                Text(this.foodInfo.calories.toString())
                    .fontColor(this.getCalorieColor())
                    .fontSize(65)
                Text($r('app.string.calorie_with_kcal_unit', ''))
                    .fontSize(20)
                    .margin({ bottom: 10 })
            }
            .margin({ top: 25, bottom: 25 })
            .alignItems(VerticalAlign.Bottom)

            Text(this.highCalories ? $r('app.string.high_calorie_food') : $r('app.string.low_calorie_food'))
                .fontSize(13)
                .fontColor('#313131')

            Progress({ value: this.foodInfo.calories, total: this.totalCalories, style: ProgressStyle.Linear })
                .style({ strokeWidth: 24 })
                .color(this.getCalorieColor())
                .margin({ top: 18 })
        }
        .cardStyle()
    }

    getCalorieColor() {
        return this.highCalories ? $r('app.color.high_calorie') : $r('app.color.low_calorie')
    }
}

@Styles function cardStyle () {
    .height('100%')
    .padding({ top: 20, right: 20, left: 20 })
    .backgroundColor(Color.White)
    .borderRadius(12)
}
