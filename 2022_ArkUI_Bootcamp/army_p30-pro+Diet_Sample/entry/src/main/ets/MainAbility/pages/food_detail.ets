import { FoodInfo } from '../model/type/DietType'
import { getFoodInfo } from '../model/data/DataUtil'
import { Record } from './components/detail/dialog'
import { NutritionElement } from '../model/DataModels'
import { BreakPointType } from '../utils/BreakpointSystem'

import { PageTitle } from '../pages/components/detail/page_title'
import { FoodImageDisplay } from '../pages/components/detail/food_image_display'
import { ContentTable } from './components/detail/content_table'
import { CaloriesProgress } from './components/detail/calories_progress'
import { NutritionPercent } from './components/detail/nutrition_percent'
import { NutritionPieChart } from './components/detail/nutrition_pie_chart'

@Entry
@Component
struct FoodDetail {
    @StorageProp('currentBreakpoint') currentBreakpoint: string = 'sm'
    private foodInfo: FoodInfo = getFoodInfo()
    private nutritionElements: NutritionElement[]
    dialogController: CustomDialogController = new CustomDialogController({
        builder: Record({ foodInfo: this.foodInfo }),
        autoCancel: true,
        alignment: DialogAlignment.Bottom,
        offset: { dx: 0, dy: -20 },
        customStyle: true
    })

    aboutToAppear() {
        let total = this.foodInfo.protein + this.foodInfo.fat + this.foodInfo.carbohydrates
        this.nutritionElements = [
            new NutritionElement($r('app.string.diet_record_protein'), this.foodInfo.protein, '#ff9421'),
            new NutritionElement($r('app.string.diet_record_fat'), this.foodInfo.fat, '#ffd100'),
            new NutritionElement($r('app.string.diet_record_carbohydrates'), this.foodInfo.carbohydrates, '#4cd041')
        ]
        let lastEndAngle = -0.5 * Math.PI
        this.nutritionElements.forEach((value) => {
            let percent = value.weight / total
            value.percent = Math.round(percent * 100)
            value.beginAngle = lastEndAngle
            value.endAngle = (percent * 2 * Math.PI) + lastEndAngle
            lastEndAngle = value.endAngle
            return value
        })
    }

    build() {
        Scroll() {
            Column() {
                PageTitle()
                FoodImageDisplay({ foodInfo: this.foodInfo })
                Swiper() {
                    ContentTable({ foodInfo: this.foodInfo })
                    CaloriesProgress({ foodInfo: this.foodInfo })
                    NutritionPercent({ foodInfo: this.foodInfo, nutritionElements: this.nutritionElements })
                    NutritionPieChart({ foodInfo: this.foodInfo, nutritionElements: this.nutritionElements })
                }
                .indicator(new BreakPointType({ sm: true, md: false, lg: false }).getValue(this.currentBreakpoint))
                .displayCount(new BreakPointType({ sm: 1, md: 2, lg: 3 }).getValue(this.currentBreakpoint))
                .clip(new Rect().width('100%').height('100%').radiusWidth(15).radiusHeight(15))
                .itemSpace(20)
                .height(330)
                .indicatorStyle({ selectedColor: $r('app.color.theme_color_green') })
                .margin({ top: 10, right: 10, left: 10 })

                Button($r('app.string.button_food_detail_record'), { type: ButtonType.Capsule, stateEffect: true })
                    .height(42)
                    .width('80%')
                    .margin({ top: 32, bottom: 32 })
                    .backgroundColor($r('app.color.theme_color_green'))
                    .onClick(() => {
                        this.dialogController.open()
                    })
            }
            .alignItems(HorizontalAlign.Center)
        }
        .backgroundColor('#EDF2F5')
        .height('100%')
        .align(Alignment.Top)
    }
}
