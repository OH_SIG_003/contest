## 环境及背景介绍：

项目设想是制作一个便携的水质监测器，可以直接测量水质中的ph值，浑浊度等，方便用户对于水质有关清晰的认知，满足用户的用水要求。

## 遇到的痛点：

我们原本打算通过显示测量水质的历史记录让用户的对水质的变化有个直观的显示，但是我们不论是通过给开发板上的app添加数据库还是靠生成本地文件，都以失败告终，也有试过mqtt，但是比较难实现，因此现在只有单单测试一次的结果。

## 建议及意见：

之后可能会换开发板，换了开发板之后无论是硬件还是软件开发估计都会方便一些。

