# 样例共建活动学习过程

## 项目初期	

​		在参加过第一次成长计划后，在这次样例共建能够快速搭建环境，毕竟积累了经验。编译环境这一关可谓是第一个拦路虎。在搭建环境时困难劝退了很多同学，这在我帮助很多同学搭建环境时确确实实能够感受到困难。在经过openharmony的更新3.0之后，搭建环境却变的更加的困难。也可能因为这次是标准系统的原因。在开发初期时，因为都没有真正接触过openharmony的开发，所以初期时需要学习大量资料。小组一般是参考别人的样例来实现自己的想法。在实现的过程中也经历重重困难

## 项目中期

​		在项目开发中期过程由于openharmony的结构对于我们来说过于复杂，所以在使用通讯协议中遇到大量的问题。开发过程反复的在linux和hdf接口横跳，导致浪费了大量的时间和精力。有时从早上10点开始，不停的在尝试不同的办法来实现与NFC模块通讯，到晚上凌晨也没有搞定。这一次活动对我来说可谓是十分艰辛。但在这一过程也学习到很多新知识，包括linux内核的结构以及注册设备与驱动入口等概念，以及代码应该如何规范书写。



## 项目后期

在项目后期过程中，为了实现最初的目标，小组还在努力的将模块接上数字选择器，以及数据库实现互联功能。虽然快接近结项。但是小组成员也没有因为时间问题而选择放弃或是懈怠。在最后的时间里也选择尝试不同的方法克服困难，虽然不尽人意，但终究努力过。

## 对活动的感受

这次活动虽然没有达到最终计划书的效果，但在于开发的过程中学习到了许多新知识，也感受到了团队的重要性。在一个完整项目中，多人参与往往能发散大家的思维，让小组能够有更多的想法和点子相互交流分享，在遇到难题时也可以一起解决问题，这次活动让我切实的感受到小组成员之间交流合作的重要性。希望能将这一次积累的知识运用在下一次活动当中。

## 对于openharmony社区建议

​	本次活动可以很深刻的体会到目前社区在面对不同群体的教学还是比较缺乏的。在面对大学生来说，现有的资料教程还不够详细，因为大学生不同年级对于技术掌握程度不同，越是初年级的同学，对于项目开发知识缺乏就越多。在这次活动搭建环境帮助同学的过程中也能深刻体会到，不同的同学对于搭建环境步骤的理解不同。但是最终而言，越是勤奋努力的同学，知识掌握的也越多。尽管项目没有完项，但是这一部分知识也可以为下一次活动打下基础。所以个人建议，openharmony社区能够提供面向不同群体的教学。以及可以提供文本式的样例开发步骤示例。对于视频而言也可以贴合时代，尽量将教学视频内容精简，不要过长