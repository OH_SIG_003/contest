####环境及背景介绍：
想要实现通过uniapp和js代码实现的能够控制硬件工作的app，而自身并没有和硬件接触，硬件分散在组员那边，所以计划先进行软件制作，再进行软件连接硬件的代码工作。
####遇到的痛点：
由于目前还没有进行软件硬件连接，痛点是针对制作软件来讲的。
最大的痛点就是毫无方向，网上大把js的视频，大把uniapp的视频，在学uniapp的同时发现还要学vue和ajax，学习毫无方向，时间又很紧迫。
解决的方法就是先了解一些很基本的东西，再去找样例看，自己的软件用不用的上这个功能，这个功能是怎么实现的，从而去学习这方面的相关知识加快自己的制作软件的效率，但同时也使我的底层知识并不牢固，所以在制作软件的同时，也要慢慢填补底层知识。
####建议及意见：
希望能有比较系统的教程及其相关的学习方向，在初入这个项目的制作时，就算可以找老师问问题，我也是完全一头雾水的，没有学习的方向，对OpenHarmony也没有什么深入的了解，如果能有一定的学习方向的提示，可以帮助初入这方面的项目人员少走很多弯路。