# 心得体会

## 环境及背景介绍

### 背景

 现市面上的蒸蛋器功能比较单一，每次蒸蛋都需要使用者放入鸡蛋和水，等待鸡蛋煮熟。但这些动作太琐碎了，琐碎的东西会消磨人的精力。有时使用者会忘记放入鸡蛋和水从而错失了早晨一颗美味且营养丰富的水煮蛋。为了使人们不会因为忘记放鸡蛋且时间匆忙而错失早晨的一颗水煮蛋，我们想开创此项目——摩天轮智能蒸蛋器。

##  遇到的痛点

+ 项目上使用unionpi_tiger开发板,网络上参考资料较少
+ 先前未接触过OpenHarmony,很多东西要重新学习
+ 对硬件知识了解不足,一些想法不知道如何转化成实体

##  建议及意见：

+ 有充足的参考样例
+ 活跃的论坛