# applications_genkipi_sample

## 介绍

genkipi 使用范例，该项目不可独立运行，需要参考此链接进行开发环境准备及源代码获取编译：https://gitee.com/genkipi/genkipi_manifest




## 安装教程

将项目下载到源代码根目录的applications中，如`applications/genkipi`：

```
applications
└── genkipi
    ├── app
    │   ├── BUILD.gn
    │   ├── env_base
    │   ├── hello_world
    │   ├── led_gpio
    │   └── led_pwm
    ├── CMakeLists.txt
    ├── LICENSE
    └── README.md
```



## 使用说明

可通过修改app下的BUILD.gn文件，决定编译哪些应用：

```gn
import("//build/lite/config/component/lite_component.gni")

lite_component("app") {
    features = [
        "hello_world",
#        "led_gpio",
#        "led_pwm",
        "env_base",
    ]
}
```

代表编译`hello_world`和`env_base`两个应用。

项目`env_base`提供了默认热点和Web主页功能，在Web主页可进行AP热点查看修改、Wifi配置及固件烧录等功能。

默认WIFI名称：itcast

默认WIFI密码：1234567

