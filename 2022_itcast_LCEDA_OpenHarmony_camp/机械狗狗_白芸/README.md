# 基于OpenHarmony鸿蒙操作系统的机器狗狗~


## 设计环节

主控芯片：HI3861, 支持16路舵机控制,GPIO,ADC,PWM等操作。
工作电压：7.4V~ 20V 带过热过流保护。
电源模块：7.4V锂电池和线性稳压芯片将7.4V电源降压到5V以及3.3V对舵机及主控板上的相应原件进行供电。
舵机控制：PCA9685驱动模块。

文档固件及其资料：https://pan.baidu.com/s/1OztwrWfjke83tJw3oP9gRQ 提取码：qu2s 
环境搭建（linux）：https://robot.czxy.com/ohos/day04/env_intro.html
源码获取：https://robot.czxy.com/ohos/day04/source.html
solidworks模型，原理图及pcb设计：https://oshwhub.com/baibaiyun/ji-xie-gou-gou
固件烧录：使用Hiburn.exe 烧录。

其他：根据solidworlks机械狗的设计垢面相应螺丝及外观材料（为节省成本使用亚克力切割，后续会改进外观以及增加功能）。

## 学习心得

一开始没想到一路这么曲折，在学习的过程中遇到了好多问题，为了节约成本使用的亚克力切割，设计过程中把小腿的部分设计成爱心形状，结果连接的地方太薄，导致在完成时准备实验行走时，碎了...在pcb设计过程中买回来的开关对不上导致复位不好用，只能斜过来焊接，大家一定要细心细心，幸亏用万用表测出来，把开关斜着焊上去了（已经换了封装）。软件这次我是第一次接触python，学到了不少知识，以后还会接着学习，并且把学到的东西加到狗狗身上。
总之，还是得多实践多学习，软件方面还多不是很明白。



