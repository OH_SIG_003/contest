# ViewSwitcher

#### 项目介绍
    - 该组件是一个动画切换视图的OpenHarmony组件库

#### 项目演示
   
   ![输入图片说明](./screenshot/demo.gif)


##### 基础用法
   ```java
   private arr: string[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];

   @Builder makeView(mData: string[], index: number) {
    Text('Hello:' + mData[index])
      .width('100%')
      .height(80)
      .fontSize(20)
      .fontWeight(600)
      .textAlign(TextAlign.Center)
      .backgroundColor('#EBEBEB')
  }

  ViewSwitcher({
       mData: this.arr,
       makeView: this.makeView as Function,
       width: '50%',
       height: 80,
       isShowNextAnim: true,
       onInflateComplete: (switcher: ViewSwitcher)=>{
         this.switcher = switcher
       },
       onClickListener: (index: number) => {
         console.log('click:'+index)
       }
   })

   //上一条
   this.switcher.showPrevious()
   //下一条
   this.switcher.showNext()
   //自动播放，里面的参数true表示向下做切换动画，false表示向上做切换动画
   this.switcher.startAutoPlay(true)
   //停止自动播放
   this.switcher.stopAutoPlay()

   ```   

#### 接口及属性
| **属性**                             | **必填** | **说明**                                                   |
| ------------------------------------ | :------: | --------------------------------------------------------- |
| playIntervalTime:  number            |    否    | 自动播放间隔时间，默认3000ms                                |
| duration: number                     |    否    | 切换动画时长，默认500                                       |
| curve: Curve                         |    否    | 动画变化曲线，默认Curve.EaseOut                             |
| mData: Array<any>                    |    是    | 原始数据                                                   |
| makeView: Function                   |    是    | Item视图构建函数                                            |
| width: Length                        |    否    | 控件宽度,默认‘100%’                                         |
| height: Length                       |    否    | 控件高度,默认‘100%’                                         |
| isShowNextAnim: boolean              |    否    | 是否从下向上做动画，默认true                                       |
| onInflateComplete: Function          |    否    | 为回调给调用者一个ViewSwitcher对象，方便做后续操作, 函数结构:(obj: ViewSwitcher) => void      |
| onClickListener: Function            |    否    | 点击事件，函数结构：(index: number) => void                  |


#### 版权和许可信息

```
Copyright (c) 2022 gaojianming

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```


    